package th.ac.ku.cpe.seniorproject.senior_project_note_app;

public class HTRInputTransformer {
    public float[][][][] transform(byte[] input) {
        final int TARGET_WIDTH = 1024;
        final int TARGET_HEIGHT = 128;
        float[][][][] transformedInput = new float[1][TARGET_WIDTH][TARGET_HEIGHT][1];
        int index = 0;
        for (int h = 0; h < TARGET_HEIGHT; h++) {
            for (int w = 0; w < TARGET_WIDTH; w++) {
                int p = input[index] & 0xFF;
                float fp = p;
                transformedInput[0][w][h][0] = fp / 255.0f;
                index++;
            }
        }
        return transformedInput;
    }
}