package th.ac.ku.cpe.seniorproject.senior_project_note_app;

import android.content.Context;
import android.content.res.AssetFileDescriptor;

import java.io.IOException;
import java.nio.ByteBuffer;

public class AssetLoader {
    final Context context;

    public AssetLoader(Context context) {
        this.context = context;
    }

    public ByteBuffer loadAsset(String assetPath) throws IOException {
        AssetFileDescriptor modelRawData = context.getAssets().openFd(assetPath);
        return TFUtils.inputStreamToByteBuffer(modelRawData.createInputStream(), (int)modelRawData.getLength());
    }
}
