package th.ac.ku.cpe.seniorproject.senior_project_note_app;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.util.Log;

import androidx.annotation.NonNull;

import org.tensorflow.lite.TensorFlowLite;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.loader.FlutterLoader;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.view.FlutterMain;

public class MainActivity extends FlutterActivity {

  private final String TF_CHANNEL = "th.ku.cpe.seniorproject/tf";
  private HTRModel1 htrModel;
  private ExecutorService tfExecutor;

  private final String SFD_CHANNEL = "th.ku.cpe.seniorproject/sfd";

  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
    GeneratedPluginRegistrant.registerWith(flutterEngine);

    tfExecutor = Executors.newSingleThreadExecutor();
    setupTensorflowChannel(flutterEngine.getDartExecutor().getBinaryMessenger());

    setupSaveFileDialogChannel(flutterEngine.getDartExecutor().getBinaryMessenger());
  }

  private void setupTensorflowChannel(BinaryMessenger binaryMessenger) {
    new MethodChannel(binaryMessenger, TF_CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
      @Override
      public void onMethodCall(@NonNull MethodCall methodCall, @NonNull MethodChannel.Result result) {
        switch (methodCall.method) {
          case "getRuntimeVersion": {
            handleGetRuntimeVersionMethodCalled(methodCall, result);
          } break;
          case "loadModel": {
            handleLoadModelMethodCalled(methodCall, result);
          } break;
          case "dispose": { } break;
          case "predictHTR_1": {
            Log.i("HTR", "MethodChannel 1");
            tfExecutor.submit(() -> {
              Log.i("HTR", "MethodChannel 2");
              handlePredictHTRMethodCalled(methodCall, result);
              Log.i("HTR", "MethodChannel 3");
            });
          } break;
        }
      }
    });
  }

  private void handleLoadModelMethodCalled(@NonNull MethodCall methodCall, @NonNull MethodChannel.Result result) {
    final AssetLoader assetLoader = new AssetLoader(getApplicationContext());
    htrModel = new HTRModel1(assetLoader);
    final String htrModelAssetPath = methodCall.argument("htrModelPath");
    final String ctcDecodedModelPathAssetPath = methodCall.argument("ctcDecodedModelPath");

    try {
      final String keyForHtrModelAssetPath = FlutterMain.getLookupKeyForAsset(htrModelAssetPath);
      final String keyForCtcDecodedModelPathAssetPath = FlutterMain.getLookupKeyForAsset(ctcDecodedModelPathAssetPath);
      htrModel.setup(keyForHtrModelAssetPath, keyForCtcDecodedModelPathAssetPath);
      result.success(htrModel.toString());
    } catch (IOException ex) {
      ex.printStackTrace();
      result.error(ex.getClass().getName(), null, null);
    }
  }

  private void handleGetRuntimeVersionMethodCalled(@NonNull MethodCall methodCall, @NonNull MethodChannel.Result result) {
    result.success(TensorFlowLite.runtimeVersion());
  }

  private void handlePredictHTRMethodCalled(@NonNull MethodCall methodCall, @NonNull MethodChannel.Result result) {
    final byte[] inputData = methodCall.argument("input");
    Log.i("HTR", "handlePredictHTRMethodCalled 1");
    final HTRInputTransformer transformer = new HTRInputTransformer2();
    final float[][][][] input = transformer.transform(inputData);
    Log.i("HTR", "handlePredictHTRMethodCalled 2");
    final float[] prediction = htrModel.predict(input);
    Log.i("HTR", "handlePredictHTRMethodCalled 3");
    final int[] outputData = new int[prediction.length];
    final int TOP_PATH = 1;
    for (int i = 0; i < TOP_PATH &&  i < prediction.length; i++) {
      float confidence = prediction[i];
      outputData[i] = Float.floatToIntBits(confidence);
    }
    Log.i("HTR", "handlePredictHTRMethodCalled 4");
    for (int i = TOP_PATH; i < prediction.length; i++) {
      outputData[i] = Math.round(prediction[i]);
    }
    Log.i("HTR", "handlePredictHTRMethodCalled 5");
    runOnUiThread(() -> {
      Log.i("HTR", "handlePredictHTRMethodCalled 7");
      result.success(outputData);
      Log.i("HTR", "handlePredictHTRMethodCalled 8");
    });
    Log.i("HTR", "handlePredictHTRMethodCalled 6");
  }


  /*
   * For Save File Dialog
   */
  private MethodChannel.Result sfdResultRef;
  private byte[] saveFileBytes;

  private void setupSaveFileDialogChannel(BinaryMessenger binaryMessenger) {
    new MethodChannel(binaryMessenger, SFD_CHANNEL).setMethodCallHandler(new MethodChannel.MethodCallHandler() {
      @Override
      public void onMethodCall(@NonNull MethodCall methodCall, @NonNull MethodChannel.Result result) {
        switch (methodCall.method) {
          case "save": {
            handleOpenSaveFileDialog(methodCall, result);
          } break;
          default:
            result.notImplemented();
            break;
        }
      }
    });
  }

  private void handleOpenSaveFileDialog(@NonNull MethodCall methodCall, @NonNull MethodChannel.Result result) {
    final String initialFileName = methodCall.argument("name");
    final String mime = methodCall.argument("mime");

    final Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
    intent.setType(mime);
    intent.addCategory(Intent.CATEGORY_OPENABLE);
    intent.putExtra(Intent.EXTRA_TITLE, initialFileName);
    sfdResultRef = result;
    saveFileBytes = methodCall.argument("data");
    startActivityForResult(intent, 0x11223344);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    switch (requestCode) {
      case 0x11223344: {
        if (resultCode == RESULT_OK) {
          final Uri uri = data.getData();
          if (uri != null) {
            try {
              final OutputStream outStream = new BufferedOutputStream(getContentResolver().openOutputStream(uri));
              outStream.write(saveFileBytes);
              outStream.flush();
              outStream.close();
            } catch (IOException e) {
              sfdResultRef.error("SFD/" + e.getClass().getName(), e.getMessage(), null);
            }

            String result = null;
            try (Cursor cursor = getContentResolver().query(uri, null, null, null, null)) {
              if (cursor != null && cursor.moveToFirst()) {
                result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
              }
            }
            sfdResultRef.success(result);
          } else {
            sfdResultRef.error("SFD/URI-NULL", null, null);
          }
        } else if (resultCode == RESULT_CANCELED) {
          sfdResultRef.success(null);
        } else {
          sfdResultRef.error("SFD/RESULT_ERR", null, null);
        }
      } break;
    }
  }
}
