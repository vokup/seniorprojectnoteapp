package th.ac.ku.cpe.seniorproject.senior_project_note_app;

public class Configs {
    public static int TOP_PATHS = 1;
    public static int INPUT_IMAGE_WIDTH = 1024;
    public static int INPUT_IMAGE_HEIGHT = 128;
}
