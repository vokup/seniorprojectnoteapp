package th.ac.ku.cpe.seniorproject.senior_project_note_app;


import android.content.Context;
import android.content.res.AssetFileDescriptor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class TFUtils {
    public static ByteBuffer inputStreamToByteBuffer(InputStream inputStream, int capacity) throws IOException {
        byte[] buffer = new byte[capacity];
        int offset = 0;
        int len = 0;
        do {
            len = inputStream.read(buffer, offset, capacity - offset);
            offset += len;
        } while (len != -1);

        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(capacity);
        byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer.put(buffer);
        return byteBuffer;
    }
}
