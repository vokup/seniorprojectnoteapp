package th.ac.ku.cpe.seniorproject.senior_project_note_app;

import android.content.Context;
import android.util.Log;
import org.tensorflow.lite.Interpreter;
import org.tensorflow.lite.flex.FlexDelegate;
import org.tensorflow.lite.gpu.GpuDelegate;

import java.io.IOException;

public class HTRModel1 {
    //    private GpuDelegate predictGpuDelegate;
    private Interpreter htrModelInterpreter;
    private FlexDelegate flexDelegate;
    private Interpreter ctcDecodeInterpreter;

    private AssetLoader assetLoader;

    public HTRModel1(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
    }

    public void setup(String htrModelPath, String decodeModelPath) throws IOException {
        flexDelegate = new FlexDelegate();

        htrModelInterpreter = new Interpreter(assetLoader.loadAsset(htrModelPath));
        ctcDecodeInterpreter = new Interpreter(
                assetLoader.loadAsset(decodeModelPath),
                new Interpreter.Options().addDelegate(flexDelegate)
        );
    }

    // simulate cv2.meanStdDev function
    private void normalize(float[][][] input) {
        final int elemSize = input.length * input[0].length * input[0][0].length;
        float mean = 0.0f;
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                mean += input[i][j][0];
            }
        }
        mean /= elemSize;

        float std = 0.0f;
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                float calc = input[i][j][0] - mean;
                std += calc * calc;
            }
        }

        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                input[i][j][0] -= mean;
            }
        }

        if (std > 0) {
            std /= elemSize;
            std = (float)Math.sqrt(std);
            for (int i = 0; i < input.length; i++) {
                for (int j = 0; j < input[i].length; j++) {
                    input[i][j][0] /= std;
                }
            }
        }
    }

    public float[] predict(float[][][][] htrInput) {
        if (htrInput.length > 1) {
            throw new UnsupportedOperationException("input size must be equal to 1");
        }
//        for (int i = 0; i < 5; i++) {
//            Log.i("BEFORE", String.format("%d: %f", i, htrInput[0][0][i][0]));
//        }
        Log.i("HTRModel", "Before Normalization");
        normalize(htrInput[0]);
        Log.i("HTRModel", "After Normalization");
//        for (int i = 0; i < 5; i++) {
//            Log.i("AFTER", String.format("%d: %f", i, htrInput[0][0][i][0]));
//        }
        float[][][] htrOutput = new float[1][128][71];
        Log.i("HTRModel", "Before Main Model Prediction");
        try {
            htrModelInterpreter.run(htrInput, htrOutput);
        } catch (Exception ex) {
            Log.i("HTRModel", ex.getMessage());
            ex.printStackTrace();
        }
        Log.i("HTRModel", "After Main Model Prediction");
        final int TOP_PATH = Configs.TOP_PATHS;

        float[][] decodedOutput = new float[1][TOP_PATH + TOP_PATH * 128];
        ctcDecodeInterpreter.run(htrOutput, decodedOutput);
        return decodedOutput[0];
    }
}
