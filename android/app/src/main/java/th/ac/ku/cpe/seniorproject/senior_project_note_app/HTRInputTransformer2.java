package th.ac.ku.cpe.seniorproject.senior_project_note_app;

public class HTRInputTransformer2 extends HTRInputTransformer {
    @Override
    public float[][][][] transform(byte[] input) {
        final int TARGET_WIDTH = 1024;
        final int TARGET_HEIGHT = 128;
        float[][][][] transformedInput = new float[1][TARGET_WIDTH][TARGET_HEIGHT][1];
        int index = 0;
        for (int r = 0; r < TARGET_WIDTH; r++) {
            for (int c = 0; c < TARGET_HEIGHT; c++) {
                int p = input[index] & 0xFF;
                float fp = p;
                //transformedInput[0][r][c][0] = fp / 255.0f;
                transformedInput[0][r][c][0] = fp;
                index++;
            }
        }
        return transformedInput;
    }
}