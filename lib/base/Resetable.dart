abstract class Resetable {
  Future<void> reset();
}