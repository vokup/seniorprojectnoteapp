import 'dart:async';
import 'dart:collection';

import 'package:senior_project_note_app/base/Resetable.dart';

abstract class GroupQueueWorkerContract {
  Future<void> doWork(int groupId);
}

class GroupQueueModel {
  // use for simulating a queue from splay-tree set
  final int index;
  final int timestamp;
  final int groupId;

  GroupQueueModel(this.index, this.timestamp, this.groupId);
}

class GroupQueueService implements Resetable {
  final Set<GroupQueueModel> _internalQueue;
  final Map<int, int> _groupIdToIndex;

  final _statbleDuration;
  int _internalCounter;
  bool _isRunning;
  bool _isLooperStopped;

  // for resetting
  GroupQueueWorkerContract _worker;

  GroupQueueService([int stableDuration = 3000]):
      _internalQueue = SplayTreeSet<GroupQueueModel>((k1, k2) => k1.index - k2.index),
      _groupIdToIndex = Map<int, int>(),
      _internalCounter = 1,
      _statbleDuration = stableDuration,
      _isRunning = false,
      _isLooperStopped = true;

  Future<void> start(GroupQueueWorkerContract worker) async {
    if (_isRunning) {
      return;
    } else {
      _worker = worker;
      _isRunning = true;
      _isLooperStopped = false;
    }

    return Future.doWhile(() async {
      final curEpoch = DateTime.now().millisecondsSinceEpoch;
      if (_internalQueue.isNotEmpty) {
        final front = _internalQueue.first;
        if (curEpoch - front.timestamp >= _statbleDuration) {
          _internalQueue.remove(front);
          _groupIdToIndex.remove(front.groupId);
          await worker.doWork(front.groupId);
        } else {
          await Future.delayed(Duration(seconds: 1));
        }
      } else {
        await Future.delayed(Duration(seconds: 1));
      }
      return _isRunning;
    }).whenComplete(() {
      _isLooperStopped = true;
    });
  }

  void add(int groupId) {
    final timestamp = DateTime.now().millisecondsSinceEpoch;
    _internalQueue.add(
      GroupQueueModel(
        _internalCounter,
        timestamp, 
        groupId
      )
    );
    _groupIdToIndex[groupId] = _internalCounter;
    _internalCounter++;
  }

  void remove(int groupId) {
    final index = _groupIdToIndex[groupId];
    if (index != null) {
      _internalQueue.remove(GroupQueueModel(index, null, null));
      _groupIdToIndex.remove(groupId);
    }
  }

  Future<void> _internalStop(bool fromInternal) async {
    _isRunning = false;
    if (!!!fromInternal) {
      _worker = null;
    }
    return Future.doWhile(() async {
      await Future.delayed(Duration(seconds: 1));
      return !!!_isLooperStopped;
    });
  }

  Future<void> stop() {
    return _internalStop(false);
  }

  @override
  Future<void> reset() async {
    await _internalStop(true);
    _internalQueue.clear();
    _groupIdToIndex.clear();
    start(_worker);
  }
}