class DrawableIDProviderService {
  int _counter;

  int get currentId => _counter;

  DrawableIDProviderService([ int initialId = 1 ]):
      _counter = initialId;

  void dangerousSetID(int id) { _counter = id; }

  int getID() => _counter++;
}