import 'dart:ui' as ui;

import 'package:senior_project_note_app/drawables/PenDrawable.dart';

class WordImageCreatingService {
  Future<ui.Image> create(Iterable<PenDrawable> drawables) {
    /**
     * STEP:
     * 1. Finding bound
     * 2. Shift each path relative to zero offset
     * 3. Draw each path to canvas
     */

    print("=== WordImageCreatingService ====");
    final firstDrawable = drawables.first;
    var bound = firstDrawable.getBound();
    for (final d in drawables.skip(1)) {
      bound = bound.expandToInclude(d.getBound());
    }
    print("BOUND: $bound, (${bound.width}, ${bound.height})");

    final pr = ui.PictureRecorder();
    final canvas = ui.Canvas(pr);
    canvas.scale(4, 4);

    for (final d in drawables) {
      final shiftedPath = d.path.shift(-bound.topLeft);
      canvas.drawPath(shiftedPath, d.paint);
    }

    return pr.endRecording().toImage(
        bound.width.toInt() * 4,
        bound.height.toInt() * 4
    );
  }
}