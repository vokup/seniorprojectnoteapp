import 'dart:collection';
import 'dart:math';

import 'package:flutter/painting.dart';
import 'package:senior_project_note_app/services/fonts/Base.dart';

// Aim: Calculate how 100 font size compare to 100px
class OpenSansFontScaleCalculator {

  static const double _referenceFontSize = 100.0;

  // relative to ideographic line
  static final factorOfLines = UnmodifiableListView([ 
    0.27,     // upper-case
    0.395,    // lower-case
    0.785,    // base-line
    1.0       // ideographic
  ]);

  // 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!,.'-
  final _charsFactor = <String, CharFactor> {
    "ABCDEFGHIJKLMNOPQRSTUVWXYZbdfhklt0123456789?!-": CharFactor(0, 2),
    "aceimnorsuvwxz": CharFactor(1, 2),
    "j": CharFactor(0, 3),
    "gpqy": CharFactor(1, 3),
  };

  final double _refIdeographic;

  OpenSansFontScaleCalculator():
    _refIdeographic = _initializeIdeographicDistance();

  static double _initializeIdeographicDistance() {
    final textPainter = TextPainter(
      textDirection: TextDirection.ltr,
      text: TextSpan(
        text: "Aajg",
        style: TextStyle(
          fontFamily: "MyOpenSansCondensed",
          fontSize: _referenceFontSize
        )
      )
    );
    textPainter.layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.computeDistanceToActualBaseline(TextBaseline.ideographic);
  }

  MeasuringResult measureFromString(String str) {
    double topFactor = 1.0;
    double bottomFactor = 0.0;
    for (var i = 0; i < str.length; i++) {
      for (final k in _charsFactor.keys) {
        if (k.contains(str[i])) {
          topFactor = min(topFactor, factorOfLines[_charsFactor[k].topFactorIndex]);
          bottomFactor = max(bottomFactor, factorOfLines[_charsFactor[k].bottomFactorIndex]);
          break;
        }
      }
    }

    final renderedHeight = (bottomFactor - topFactor) * _refIdeographic;

    final offsetScale = (_refIdeographic / renderedHeight) * topFactor;
    final fontScale = _referenceFontSize / renderedHeight;
    final baselineScale = (_refIdeographic / renderedHeight) * factorOfLines[2];

    return MeasuringResult(
      offsetScale,
      baselineScale,
      fontScale
    );
  }
}

class OpenSansApostropheFontScaleCalculator {

  static const double _referenceFontSize = 100.0;

  // relative to ideographic line
  static final factorOfLines = UnmodifiableListView([ 
    0.27,     // upper-case
    0.395,    // lower-case
    0.785,    // base-line
    1.0       // ideographic
  ]);

  // 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!,.'-
  final _charsFactor = <String, CharFactor> {
    "ABCDEFGHIJKLMNOPQRSTUVWXYZbdfhklt0123456789?!-": CharFactor(0, 2),
    "aceimnorsuvwxz,.'": CharFactor(1, 2),
    "j": CharFactor(0, 3),
    "gpqy": CharFactor(1, 3),
  };

  final double _refIdeographic;

  OpenSansApostropheFontScaleCalculator():
    _refIdeographic = _initializeIdeographicDistance();

  static double _initializeIdeographicDistance() {
    final textPainter = TextPainter(
      textDirection: TextDirection.ltr,
      text: TextSpan(
        text: "Aajg",
        style: TextStyle(
          fontFamily: "MyOpenSansCondensed",
          fontSize: _referenceFontSize
        )
      )
    );
    textPainter.layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.computeDistanceToActualBaseline(TextBaseline.ideographic);
  }

  double getHeightFromFontSizeAndFactor(double fontSize, double fontScale) {
    print("REF: $_refIdeographic");
    return fontSize / fontScale;
  }

  MeasuringResult measureFromString(String str) {
    double topFactor = 1.0;
    double bottomFactor = 0.0;
    for (var i = 0; i < str.length; i++) {
      for (final k in _charsFactor.keys) {
        if (k.contains(str[i])) {
          topFactor = min(topFactor, factorOfLines[_charsFactor[k].topFactorIndex]);
          bottomFactor = max(bottomFactor, factorOfLines[_charsFactor[k].bottomFactorIndex]);
          break;
        }
      }
    }

    final renderedHeight = (bottomFactor - topFactor) * _refIdeographic;

    final offsetScale = (_refIdeographic / renderedHeight) * topFactor;
    final fontScale = _referenceFontSize / renderedHeight;
    final baselineScale = (_refIdeographic / renderedHeight) * factorOfLines[2];

    return MeasuringResult(
      offsetScale,
      baselineScale,
      fontScale
    );
  }
}

