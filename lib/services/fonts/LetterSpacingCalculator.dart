import 'package:senior_project_note_app/services/fonts/Base.dart';

class OpenSansLetterSpacingCalculator implements LetterSpacingCalculator {
  @override
  double calculate(double fontSize) {
    return 1.0;
  }
}