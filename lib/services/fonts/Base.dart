class CharFactor {
  final int topFactorIndex;
  final int bottomFactorIndex;
  CharFactor(this.topFactorIndex, this.bottomFactorIndex);
}

class MeasuringResult {
  final double offsetScale;
  final double baselineScale;
  final double fontScale;
  MeasuringResult(this.offsetScale, this.baselineScale, this.fontScale);
}

abstract class FontScaleCalculator {
  MeasuringResult measureFromString(String str);
}

abstract class LetterSpacingCalculator {
  double calculate(double fontSize);
}