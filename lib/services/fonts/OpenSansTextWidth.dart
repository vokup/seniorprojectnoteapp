import 'package:flutter/painting.dart';
import 'package:senior_project_note_app/services/fonts/Base.dart';
import 'package:senior_project_note_app/services/fonts/LetterSpacingCalculator.dart';

class OpenSansTextWidth {

  final TextPainter _textPainter;
  final LetterSpacingCalculator _letterSpacingCalculator;

  OpenSansTextWidth(
    this._letterSpacingCalculator
  ):
  _textPainter = TextPainter(
    textDirection: TextDirection.ltr
  );

  double mesuringTextWidth(String str, double fontSize) {
    final textSpan = TextSpan(
      text: str,
      style: TextStyle(
        fontFamily: "MyOpenSansCondensed",
        fontSize: fontSize,
        letterSpacing: _letterSpacingCalculator.calculate(fontSize),
      )
    );

    _textPainter.text = textSpan;
    _textPainter.layout(minWidth: 0, maxWidth: double.infinity);
    
    print("MEASURING: ${_textPainter.width}, FS: $fontSize, LT: ${_letterSpacingCalculator.calculate(fontSize)}");
    return _textPainter.width;
  }
}
