import 'dart:async';
import 'dart:math';
import 'dart:ui';
import 'package:r_tree/r_tree.dart';
import 'package:senior_project_note_app/base/Resetable.dart';
import 'package:senior_project_note_app/data/DrawableGroupRepository.dart';
import 'package:senior_project_note_app/drawables/Boundable.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/libs/DrawableGrabber.dart';

class GroupStreamModel {
  final bool isAdding;
  final DrawableGroupModel groupModel;
  bool get isRemove => !!!isAdding;

  GroupStreamModel(this.isAdding, this.groupModel);
}

class WordGroupingService implements Resetable {
  static const OVERLAPPING_THRESHOLD = 60.0;

  // external
  final DrawableGroupRepository _drawableGroupRepository;

  // properties
  Stream<GroupStreamModel> get groupStream => _groupController.stream;

  // internal state
  final StreamController<GroupStreamModel> _groupController;
  final Map<int, int> _drawableIdToGroupId;
  final RTree<int> _rtree;
  final Map<int, RTreeDatum> _rtreeNode;

  int _groupIdCounter;

  WordGroupingService(
    DrawableGroupRepository drawableGroupRepository,  
    [ int initialGroupID = 1 ]
  ):
  _drawableGroupRepository = drawableGroupRepository,
  _groupIdCounter = initialGroupID,
  _drawableIdToGroupId = Map<int, int>(),
  _rtree = RTree<int>(),
  _rtreeNode = Map<int, RTreeDatum>(),
  _groupController = StreamController();

  void _publishAddGroupStream(DrawableGroupModel model) {
    _groupController.add(GroupStreamModel(true, model));
  }

  void _publishRemoveGroupStream(int groupId) {
    _groupController.add(GroupStreamModel(false, DrawableGroupModel(groupId, null, null)));
  }

  void _setDrawbleIDToGroupID(int did, int gid) {
    // print("SET DID:$did to GID: $gid");
    _drawableIdToGroupId[did] = gid;
  }

  Rectangle _rectToRectangle(Rect rect) {
    return Rectangle(rect.left, rect.top, rect.width, rect.height);
  }

  void _notifyAddRTree(Rect bound, int groupId) {
    final insertingNode = RTreeDatum(_rectToRectangle(bound), groupId);
    _rtreeNode[groupId] = insertingNode;
    _rtree.insert(insertingNode);
  }

  // notify when drawable is added.
  void notifyAdd(Drawable drawable) {
    if (drawable is Boundable) {
      final drawableBound = (drawable as Boundable).getBound();

      Set<int> mergeSet = Set<int>.from([ drawable.id ]);
      Rect mergeBound = drawableBound;
      var stillOverlapping;
      do {
        stillOverlapping = false;
        final searchBound = mergeBound.deflate(-OVERLAPPING_THRESHOLD);
        final candidateGroups = _rtree.search(_rectToRectangle(searchBound));

        // print("NotifyAdd: FOUND ${candidateGroups.length} Gruops");

        if (candidateGroups.length > 0) {
          for (final cg in candidateGroups) {
            // print("CURRENT CANDIDATE: [GID: ${cg.value}]");
            final groupId = cg.value;

            final groupModel = _drawableGroupRepository.lookup(groupId);
            _drawableGroupRepository.removeByID(groupId);

            if (groupModel != null) {
              final groupBound = groupModel.bound;
              final groundSet = groupModel.drawableIds;
              mergeBound = mergeBound.expandToInclude(groupBound);
              mergeSet = mergeSet.union(groundSet);

              _notifyDeleteRTree(groupModel.id);
              _publishRemoveGroupStream(groupModel.id);
              stillOverlapping = true;
            }
          }
        }
      } while (stillOverlapping);

      // prevent zero bounded group.
      if (mergeBound.width >= 1 && mergeBound.height >= 1) {
        // adding new group
        final newGroup = DrawableGroupModel(
            _groupIdCounter,
            mergeBound,
            mergeSet
        );
        _groupIdCounter++;

        _drawableGroupRepository.add(newGroup);

        _notifyAddRTree(newGroup.bound, newGroup.id);

        for (final did in mergeSet) {
          _setDrawbleIDToGroupID(did, newGroup.id);
        }

        _publishAddGroupStream(newGroup);
      }
    }
    // for (final g in _drawableGroupRepository.groups) {
    //   print("G${g.id}: ${g.drawableIds.toList().join(",")}");
    // }
  }

  void _notifyDeleteRTree(int groupId) {
    final node = _rtreeNode[groupId];
    _rtreeNode.remove(groupId);
    if (node != null) {
      _rtree.remove(node);
    }
  }

  // notify when drawable is deleted.
  void notifyDelete(DrawableGrabberContract drawableGrabber, int drawableId) {
    /**
     * STEP:
     *  1. Remove old group
     *  2. Get current group that belong to current drawable.
     *  3. Remove the drawable from the group
     *  4. Check new group composition of current group
     *  5. Add new group
     */

    final groupId = _drawableIdToGroupId[drawableId];
    _drawableIdToGroupId.remove(drawableId);

    // BEFORE: DrawableGroupRepository
    // final groupModel = _groups.lookup(DrawableGroupModel(groupId, null, null));
    // _groups.remove(groupModel);
    
    final groupModel = _drawableGroupRepository.lookup(groupId);
    _drawableGroupRepository.removeByID(groupId);

    _notifyDeleteRTree(groupModel.id);
    _publishRemoveGroupStream(groupModel.id);

    if (groupModel != null) {
      // recompute group of all drawables in current group
      groupModel.drawableIds.remove(drawableId);
      for (final did in groupModel.drawableIds) {
        final d = drawableGrabber.lookupByID(did);
        notifyAdd(d);
      }
    }
  }

  @override
  Future<void> reset() async {
    _rtreeNode.forEach((_, node) {
      _rtree.remove(node);
    });
    _rtreeNode.clear();
    _drawableIdToGroupId.clear();
  }
}