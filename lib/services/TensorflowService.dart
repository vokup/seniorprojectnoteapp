import 'dart:typed_data' as typed_data;
import 'package:flutter/services.dart' as service;

class TFLiteService {
  static const platform = const service.MethodChannel('th.ku.cpe.seniorproject/tf');

  static Future<String> loadModel(String htrModelPath, String ctcDecodedModelPath) async {
    try {
      String test = await platform.invokeMethod("loadModel", {
        "htrModelPath": htrModelPath,
        "ctcDecodedModelPath": ctcDecodedModelPath
      });
      return test;
    } on service.PlatformException catch (e) {
      return Future.error(e);
    }
  }

  static void dispose() async {
    await platform.invokeMethod("dispose");
  }

  static Future<String> getRuntimeVersion() {
    return platform.invokeMethod<String>("getRuntimeVersion");
  }

  static Future<typed_data.Int32List> predictHandwriting(typed_data.Uint8List input) async {
    return platform.invokeMethod<typed_data.Int32List>("predictHTR_1", {
      "input": input
    });
  }
}