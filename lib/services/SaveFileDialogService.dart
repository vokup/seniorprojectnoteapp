import 'package:meta/meta.dart';
import 'package:flutter/services.dart' as service;
import 'dart:typed_data' as typed_data;

class SaveFileDialogService {
  static const platform = const service.MethodChannel('th.ku.cpe.seniorproject/sfd');

  Future<String> save({
    @required String name,
    @required String mime,
    @required typed_data.Uint8List data
  }) {
    return platform.invokeMethod("save", {
      "name": name,
      "mime": mime,
      "data": data
    });
  }
}