import 'package:flutter/painting.dart';
import 'package:senior_project_note_app/data/ConvertedTextRepository.dart';
import 'package:senior_project_note_app/data/DrawableGroupRepository.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/drawables/PenDrawable.dart';
import 'package:senior_project_note_app/libs/DrawableGrabber.dart';
import 'package:senior_project_note_app/libs/DrawingAreaScaler.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/event.dart';
import 'package:senior_project_note_app/services/GroupQueueService.dart';
import 'package:senior_project_note_app/services/TensorflowService.dart';
import 'package:senior_project_note_app/services/WordImageCreatingService.dart';
import 'package:senior_project_note_app/services/fonts/LetterSpacingCalculator.dart';
import 'package:senior_project_note_app/services/fonts/OpenSansFontScaleCalculator.dart';
import 'package:senior_project_note_app/services/fonts/OpenSansTextWidth.dart';
import 'package:senior_project_note_app/utils/BoundUtils.dart';
import 'package:senior_project_note_app/utils/HTRInputImageSerializer.dart';
import 'package:senior_project_note_app/utils/HTRInputImageTransformerV2.dart';
import 'package:senior_project_note_app/utils/HTRPredictionTranslator.dart';

class GroupQueueWorker implements GroupQueueWorkerContract {
  final DrawableGroupRepository _drawableGroupRepository;
  final ConvertedTextRepository _convertedTextRepository;
  final DrawingAreaBloc _drawingAreaBloc;
  final AreaScaler _areaScaler;

  GroupQueueWorker(
    DrawingAreaBloc drawingAreaBloc,
    DrawableGroupRepository drawableGroupRepository,
    ConvertedTextRepository convertedTextRepository,
    AreaScaler areaScaler
  ):
  _drawingAreaBloc = drawingAreaBloc,
  _drawableGroupRepository = drawableGroupRepository,
  _convertedTextRepository = convertedTextRepository,
  _areaScaler = areaScaler;

  @override
  Future<void> doWork(int groupId) async {
      print("DrawingAreaBloc: groupStream2 [$groupId]");
      final model = _drawableGroupRepository.lookup(groupId);
      if (model != null) {
        final watch = Stopwatch();
        watch.start();

        // final grabber = DrawingAreaBlocDrawableGrabber(_drawingAreaBloc.state.drawableList.set);
        // List<Drawable> groupDrawables = model.drawableIds.map((x) {
        //   return grabber.lookupByID(x);
        // }).toList();

        final grabber = DrawingAreaBlocDrawableGrabber(_drawingAreaBloc.state.penDrawableList.set);
        final groupDrawables = List<Drawable>();
        for (final did in model.drawableIds) {
          final drawable = grabber.lookupByID(did);
          if (drawable == null) {
            // this error occur while grabbing drawable which is deleted.
            return;
          }
          groupDrawables.add(drawable);
        }

        final relatedDrawables = groupDrawables.cast<PenDrawable>();
        final convertedTextColor = relatedDrawables.last.color;
        
        final image = await WordImageCreatingService().create(relatedDrawables);

        final bound = mergeBound(relatedDrawables.map((x) => x.getBound()));

        print("BOUND: $bound"); 
        print("IMAGE: ${image.width} ${image.height}");

        final transformer = HTRInputImageTransformerV2();
        final transformedImage = await transformer.transform(image);
        // _drawingAreaBloc.add(AddTestImage(transformedImage));

        final serializer = HTRInputImageSerializer();
        final serializedImage = await serializer.serialize(transformedImage);
        print("GQW: Predicting");
        final predictedData = await TFLiteService.predictHandwriting(serializedImage);
        print("GQW: Predicted");

        // final outputTransformer = FlorHTRPredictionOutputTransformer();
        // final outputTransformer = FlorResHTRPredictionOutputTransformer();
        final outputTransformer = FlorResHTRApostrophePredictionOutputTransformer();
        final predictedResult = outputTransformer.transform(predictedData);
        print(predictedResult);

        watch.stop();
        print("TIME: ${watch.elapsedMilliseconds}");

        final predictedWord = predictedResult[0].word;

        if (predictedWord.length <= 0) {
          // unable to recognize
          return;
        }

        // final fsc = OpenSansFontScaleCalculator();
        final fsc = OpenSansApostropheFontScaleCalculator();
        final measuringResult = fsc.measureFromString(predictedWord);

        // doing line-snapping
        var calculatedBound = bound.height;
        var calculatedOffset = bound.topLeft - Offset(0, measuringResult.offsetScale * calculatedBound);
        var calculatedFontSize = measuringResult.fontScale * calculatedBound;
        var calculatedBaselineOffset = calculatedOffset + Offset(0, measuringResult.baselineScale * calculatedBound);

        for (final ct in _convertedTextRepository.convertedTexts) {
          final otherBound = ct.textBound;
          final curBaseline = calculatedBaselineOffset;
          final otherBaseline = ct.baseline;
          final baselineDist = (curBaseline.dy - otherBaseline.dy).abs();
          final boundDist = bound.left - otherBound.right;
          final fontSizeDist = (calculatedFontSize - ct.fontSize).abs();
          print("Try snapping: ${ct.text}, font: $fontSizeDist baseline: $baselineDist, bonudDist: $boundDist, ${ct.textBound.height}");

          if (baselineDist <= _areaScaler.getTargetY(25)) {
            if (boundDist <= _areaScaler.getTargetX(200)) {
              if (fontSizeDist <= 40) {
                print("case1");
                calculatedFontSize = ct.fontSize;
                calculatedOffset = Offset(calculatedOffset.dx, ct.offset.dy);
                // need to calculate new bound for PDF
                // for string "abc" and having font size "x" what is it bound ?
                final newBound = fsc.getHeightFromFontSizeAndFactor(calculatedFontSize, measuringResult.fontScale);
                print("NEW BOUND [$predictedWord]: ${measuringResult.fontScale} $calculatedFontSize, $calculatedBound, $newBound");
                // calculatedBound = newBound;
                calculatedBound = newBound;
              } else {
                // this should be correct
                calculatedOffset = Offset(calculatedOffset.dx, calculatedOffset.dy + (ct.baseline.dy - calculatedBaselineOffset.dy));
              }
              calculatedBaselineOffset = calculatedOffset + Offset(0, measuringResult.baselineScale * calculatedBound);
            }
          }
        }

        // because `calculatedOffset` is just logical offset
        // but we need physical offset which is the real offset that displaying to the screen.
        final displayingTextYOffset = calculatedOffset.dy + (measuringResult.offsetScale * calculatedBound);
        final curConvertedText = ConvertedTextModel(
          id: groupId, 
          drawables: List<Drawable>.from(groupDrawables), 
          text: predictedWord, 
          offset: calculatedOffset, 
          baseline: calculatedBaselineOffset, 
          fontSize: calculatedFontSize,
          color: convertedTextColor,
          textBound: Rect.fromLTWH(
            calculatedOffset.dx, 
            displayingTextYOffset,
            OpenSansTextWidth(OpenSansLetterSpacingCalculator())
              .mesuringTextWidth(predictedWord, calculatedFontSize), 
            calculatedBound
          )
        );
        // make sure that group is still valid.
        if (_drawableGroupRepository.lookup(groupId) != null) {
          _convertedTextRepository.add(curConvertedText);
          _drawingAreaBloc.add(
            AddConvertedText(
              curConvertedText.text, 
              curConvertedText.fontSize,
              // this is logical offset
              curConvertedText.offset,
              curConvertedText.color,
              curConvertedText.textBound,
              List<Drawable>.from(groupDrawables),
              curConvertedText.id
            )
          );
        } else {
          print("Group $groupId is not valid while adding converted text");
        }
      }
      print("FINISHED");
  }
}