import 'dart:math';

import 'package:flutter/material.dart';
import 'package:senior_project_note_app/drawables/Boundable.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';

class PenDrawable extends Drawable implements Boundable {

  Color get color => paint.color;
  bool get isConvertingPen => !!!isNormalPen;

  final Path path;
  final Paint paint;
  final List<Point<double>> points;
  final double sx;
  final double sy;
  final bool isNormalPen;

  final double _halfThickness;

  double cx;
  double cy;
  Rect _boundingRect;

  PenDrawable({
    @required int drawableId,
    bool isNormalPen = true,
    Color color = Colors.black, 
    double strokeWidth = 1.0, 
    List<Point<double>> points,
    this.sx = 0.0,
    this.sy: 0.0,
    double cx,
    double cy,
    Path path,
    Paint paint,
    Rect boundingRect
  }) : 
  this.points = points ?? _createPoints(sx, sy),
  this.path = path ?? _createPath(sx, sy),
  this.paint = paint ?? _createPaint(strokeWidth, color),
  this.cx = cx ?? sx,
  this.cy = cy ?? sy,
  this._boundingRect = boundingRect ?? Rect.fromLTRB(sx, sy, sx, sy),
  this.isNormalPen = isNormalPen,
  _halfThickness = strokeWidth / 2,
  super(drawableId);

  static List<Point<double>> _createPoints(double sx, double sy) {
    return List<Point<double>>()..add(Point<double>(sx, sy));
  }

  static Path _createPath(double sx, double sy) {
    return Path()..moveTo(sx, sy);
  }

  static Paint _createPaint(double strokeWidth, Color color) {
    final paint = Paint();
    paint.style = PaintingStyle.stroke;
    paint.strokeWidth = strokeWidth;
    paint.color = color;
    return paint;
  }

  factory PenDrawable.fromPoints({
    @required int id,
    @required bool isNormalPen,
    double sx,
    double sy,
    Color color,
    double thickness,
    List<Point<double>> points
  }) {
    final pd = PenDrawable(
      drawableId: id,
      isNormalPen: isNormalPen,
      sx: sx, 
      sy: sy,
      color: color,
      strokeWidth: thickness
    );
    Point<double> prev = points[0];
    for (final p in points.sublist(1)) {
      double rx = p.x - prev.x;
      double ry = p.y - prev.y;
      pd.addRelativePoint(Point<double>(rx, ry));
      prev = p;
    }
    
    return pd;
  }

  PenDrawable copyWith({
    bool isNormalPen,
    double sx,
    double sy,
    List<Point> points,
    double cx,
    double cy,
    Path path,
    Paint paint,
    Rect boundingRect
  }) {
    return PenDrawable(
      drawableId: this.id,
      isNormalPen: isNormalPen ?? this.isNormalPen,
      sx: sx ?? this.sx, 
      sy: sy ?? this.sy, 
      points: points ?? this.points, 
      cx: cx ?? this.cx, 
      cy: cy ?? this.cy,
      path: path ?? this.path,
      paint: paint ?? this.paint,
      boundingRect: boundingRect ?? this.getBound()
    );
  }

  void _updateBoundingRect() {
    final strokeScaler = 16;
    final l = min(_boundingRect.left, cx - _halfThickness * strokeScaler);
    final t = min(_boundingRect.top, cy - _halfThickness * strokeScaler);
    final r = max(_boundingRect.right, cx + _halfThickness * strokeScaler);
    final b = max(_boundingRect.bottom, cy + _halfThickness * strokeScaler);
    this._boundingRect = Rect.fromLTRB(l, t, r, b);
  }

  void addRelativePoint(Point<double> point) {
    this.cx += point.x;
    this.cy += point.y;
    _updateBoundingRect();
    // print("current: $cx $cy");
    this.points.add(Point<double>(this.cx, this.cy));
    this.path.relativeLineTo(point.x, point.y);
  }

  @override
  void draw(Canvas canvas, Size size) {
    // because we keep aspect ratio 
    // then scaled width and height will have the same value.

    /*
     * BAD CODE: THIS IS HOTFIX
     */
    final scale = 3508.0 / 842.0;

    final oriStrokeWidth = paint.strokeWidth;
    paint.strokeWidth = oriStrokeWidth * scale;
    canvas.drawPath(path, paint);
    paint.strokeWidth = oriStrokeWidth;

    // for debuging
    // if (!!!this.isNormalPen) {
      // canvas.drawRect(
      //     _boundingRect,
      //     Paint()
      //       ..style = PaintingStyle.stroke
      //       ..color = Colors.red
      //       ..strokeWidth = 1 * scale
      // );
    // }
  }

  @override
  Rect getBound() {
    return _boundingRect;
  }
}