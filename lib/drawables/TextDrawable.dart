import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:senior_project_note_app/drawables/Boundable.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';

class TextDrawable extends Drawable implements Boundable {
  final TextPainter _painter;
  final Offset offset;
  final Rect _renderedBound;
  final int groupId;

  String get text => (_painter.text as TextSpan).text;
  double get fontSize => (_painter.text as TextSpan).style.fontSize;
  Color get color => (_painter.text as TextSpan).style.color;
  String get fontFamily => (_painter.text as TextSpan).style.fontFamily;

  TextDrawable(int id, {
    @required String text, 
    @required double fontSize, 
    @required this.offset,
    @required Rect renderedBound,
    @required Color color,
    this.groupId = -1 // -1 is used to indicate a special group which came from saved file
  }):
  _renderedBound = renderedBound,
  _painter = TextPainter(
    textDirection: TextDirection.ltr,
    text: TextSpan(
      text: text,
      // text: "ABCabc012",
      style: TextStyle(
        color: color,
        fontFamily: "MyOpenSansCondensed",
        fontSize: fontSize
      )
    )
  )..layout(minWidth: 0, maxWidth: double.infinity),
  super(id);

  @override
  void draw(Canvas canvas, Size size) {
    _painter.paint(canvas, this.offset);
    // canvas.drawRect(_renderedBound, 
    //   Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1
    //   ..color = Colors.black
    // );
  }

  @override
  Rect getBound() {
    // return Rect.fromLTWH(0, 0, _painter.width, _painter.height);
    return _renderedBound;
  }
}