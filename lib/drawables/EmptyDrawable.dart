import 'dart:ui';

import 'package:senior_project_note_app/drawables/Drawable.dart';

class EmptyDrawable extends Drawable {

  EmptyDrawable(): super(-1);

  @override
  void draw(Canvas canvas, Size size) { }
}