import 'package:flutter/material.dart';

abstract class Drawable {
  int id;
  Drawable(this.id);
  void draw(Canvas canvas, Size size);
}