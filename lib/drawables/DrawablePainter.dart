import 'package:flutter/material.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/libs/DrawingAreaScaler.dart';

class DrawablePainter extends CustomPainter {

  final Iterable<Drawable> _drawables;
  final AreaScaler _areaScaler;

  DrawablePainter({ 
    @required Iterable<Drawable> drawables,
    @required AreaScaler areaScaler
  })
  :
  _drawables = drawables,
  _areaScaler = areaScaler;

  @override
  void paint(Canvas canvas, Size size) {
    if (!!!_areaScaler.isSameWorkingAreaSize(size.width, size.height)) {
      _areaScaler.setWorkingAreaSize(size.width, size.height);
    }

    canvas.scale(1 / _areaScaler.calculatedScaleWidth);
    for (var drawable in this._drawables) {
      drawable.draw(canvas, size);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}