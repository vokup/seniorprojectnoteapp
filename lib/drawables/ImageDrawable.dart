import 'dart:math';
import 'dart:ui';

// prevent Image class collision between 'dart:ui' and 'material'
import 'package:flutter/material.dart' as material;
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/Boundable.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';

class ImageDrawable extends Drawable implements Boundable {
  static const double RESIZING_SIZE = 12;

  final Offset offset;
  final Size size;
  final Image image;
  final bool showControlBox;

  final Rect _imageSourceRect;
  final Rect _imageDestRect;
  final Paint _imagePaint;

  ImageDrawable({
    @required int drawableId,
    @required this.offset,
    @required this.size,
    @required this.image,
    this.showControlBox = false,
  }):
  _imageSourceRect = Rect.fromLTWH(0, 0, image.width.toDouble(), image.height.toDouble()),
  _imageDestRect = Rect.fromLTWH(
    offset.dx, 
    offset.dy, 
    size.width, 
    size.height
  ),
  _imagePaint = Paint(),
  super(drawableId);

  @override
  void draw(Canvas canvas, Size canvasSize) {
    final scaledDestRect = Rect.fromLTWH(
      _imageDestRect.left, 
      _imageDestRect.top, 
      _imageDestRect.width, 
      _imageDestRect.height
    );

    canvas.drawImageRect(image, _imageSourceRect, scaledDestRect, _imagePaint);
    if (showControlBox) {
      _drawImageControlBox(canvas, canvasSize);
    }
  }

  @override
  Rect getBound() {
    return _imageDestRect;
    // return Rect.fromLTWH(offset.dx, offset.dy, size.width, size.height);;
  }

  void _drawImageControlBox(Canvas canvas, Size canvasSize) {
    var imagePaint = Paint()
      ..color = material.Colors.lightBlue
      ..strokeWidth = 1
      ..style = PaintingStyle.stroke;
  
    canvas.drawRect(_imageDestRect.deflate(0), imagePaint);

    final double width = size.width;
    final double height = size.height;
    final double halfWidth = width / 2.0;
    final double halfHeight = height / 2.0;

    var resizingPaint = Paint()
      ..color = material.Colors.lightBlue
      ..style = PaintingStyle.fill;

    final scale = canvasSize.width / 3508.0;  
    final scaledResizingCircle = RESIZING_SIZE / scale;

    canvas.drawCircle(offset, scaledResizingCircle, resizingPaint);
    canvas.drawCircle(offset + Offset(halfWidth, 0), scaledResizingCircle, resizingPaint);
    canvas.drawCircle(offset + Offset(width, 0), scaledResizingCircle, resizingPaint);

    canvas.drawCircle(offset + Offset(0, halfHeight), scaledResizingCircle, resizingPaint);
    canvas.drawCircle(offset + Offset(width, halfHeight), scaledResizingCircle, resizingPaint);

    canvas.drawCircle(offset + Offset(0, height), scaledResizingCircle, resizingPaint);
    canvas.drawCircle(offset + Offset(halfWidth, height), scaledResizingCircle, resizingPaint);
    canvas.drawCircle(offset + Offset(width, height), scaledResizingCircle, resizingPaint);
  }

  ImageDrawable copyWith({
    int drawableId,
    Image image,
    Offset offset,
    Size size,
    bool showControlBox
  }) {
    return ImageDrawable(
      offset: offset ?? this.offset,
      size: size ?? this.size,
      // prevent image changing, a user must remove image before adding a new one.
      image: image ?? this.image,
      showControlBox: showControlBox ?? this.showControlBox,
      drawableId: drawableId ?? this.id
    );
  }
}