class _IterableConverterIterator<U, V> extends Iterator<V> {
  @override
  V get current => _current;

  final Iterator<U> source; 
  final V Function (U) converter;
  V _current;

  _IterableConverterIterator(this.source, this.converter);

  @override
  bool moveNext() {
    final smn = source.moveNext();
    if (smn) {
      _current = converter(source.current);
    }
    return smn;
  }
}

class IterableConverter<U, V> extends Iterable<V> {
  @override
  Iterator<V> get iterator => _IterableConverterIterator<U, V>(this.source.iterator, this.converter);

  final Iterable<U> source;
  final V Function (U) converter;

  IterableConverter(this.source, this.converter);
}