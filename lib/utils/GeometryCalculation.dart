import 'dart:math';

import 'package:flutter/material.dart';

class GeometryCalculation {
  static double _distSquare(Point<num> a, Point<num> b) {
    return pow(a.x - b.x, 2) + pow(a. y- b.y, 2);
  }

  static double _dotProduct(Point<num> a, Point<num> b) {
    return (a.x * b.x) + (a.y * b.y);
  }

  // source: https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
  static double pointToLineDistanceSquare(Point<double> s, Point<double> e, Point<double> p) {
    final l2 = _distSquare(s, e);
    if (l2 == 0) {
      return _distSquare(p, s);
    }
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point p onto the line. 
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    // We clamp t from [0,1] to handle points outside the segment vw.
    final esx = e.x - s.x;
    final esy = e.y - s.y;
    final t = max(0, min(1, _dotProduct(Point(p.x - s.x, p.y - s.y), Point(esx, esy)) / l2));
    final projection = Point(s.x + t * (esx), s.y + t * (esy));
    return _distSquare(p, projection);
  }

  static bool isInBound(Rect rect, Point<double> p) {
    return !!!((p.x < rect.left) || (p.x > rect.right) || (p.y < rect.top) || (p.y > rect.bottom));
  }
}