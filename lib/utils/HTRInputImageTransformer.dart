import 'dart:math';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:image/image.dart' as image;

class HTRInputImageTransformer {
  Future<ui.Image> transform(ui.Image wordImage) async {
    const TARGET_WIDTH = 1024;
    const TARGET_HEIGHT = 128;

    final pngImage = await wordImage.toByteData(format: ui.ImageByteFormat.png);
    var workingImage = image.decodePng(pngImage.buffer.asUint8List());

    final scale = max(
        workingImage.width / TARGET_WIDTH,
        workingImage.height / TARGET_HEIGHT
    );

    final scaledWidth = max(min(TARGET_WIDTH, workingImage.width / scale), 1).toInt();
    final scaledHeight = max(min(TARGET_HEIGHT, workingImage.height / scale), 1).toInt();

    var inputImage = image.Image(1024, 128, channels: image.Channels.rgb);
    inputImage.fill(Colors.white.value);
    image.drawImage(
      inputImage,
      workingImage,
      dstX: 0,
      dstY: 0,
      dstW: scaledWidth,
      dstH: scaledHeight,
    );

    inputImage = image.copyRotate(inputImage, 90.0);
    inputImage = image.flipHorizontal(inputImage);

    final codec =  await ui.instantiateImageCodec(image.encodePng(inputImage));
    final frame = await codec.getNextFrame();
    return frame.image;
  }
}