abstract class TimestampHelperContract {
  int getTimestamp();
}

class TimestampHelper extends TimestampHelperContract {
  @override
  int getTimestamp() {
    return DateTime.now().toUtc().millisecondsSinceEpoch;
  }
}