import 'dart:typed_data' as typed_data;
import 'dart:ui' as ui;

class HTRInputImageSerializer {
  Future<typed_data.Uint8List> serialize(ui.Image image) async {
    final rawImage = await image.toByteData(format: ui.ImageByteFormat.rawRgba);
    final rawImageUint8List = rawImage.buffer.asUint8List();
    final result = typed_data.Uint8List(image.width * image.height);

    for (var i = 0; i < image.width * image.height; i++) {
      final offset = i * 4;
      final lr = rawImageUint8List[offset] * 0.2126;
      final lg = rawImageUint8List[offset + 1] * 0.7152;
      final lb = rawImageUint8List[offset + 2] * 0.0722;
      final lgray = lr + lg + lb;
      var roundedGray = lgray.round();
      if (roundedGray > 255) {
        roundedGray = 255;
      }
      result[i] = roundedGray;
//      if (i < 5) {
//        print("SERIALIZER $i: ${result[i]}");
//      }
    }
    return result;
  }
}