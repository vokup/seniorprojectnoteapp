import "dart:typed_data" as typed_data;

class HTRWordPredictionResult {
  final double confidence;
  final String word;
  HTRWordPredictionResult(this.confidence, this.word);

  @override
  String toString() {
    return "($word, $confidence)";
  }
}

abstract class HTRPredictionOutputTransformer {
  List<HTRWordPredictionResult> transform(List<int> predictionOutput);
}

class FlorHTRPredictionOutputTransformer implements HTRPredictionOutputTransformer {
  static const CHARSET = "¶¤0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#\$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ ";
  static const String PAD_TK = "¶";
  static const String UNKNOWN_TK = "¤";

  List<HTRWordPredictionResult> transform(List<int> predictionOutput) {
    const TOP_PATH = 1;
    const SEGMENT_SIZE = 128;
    final result = List<HTRWordPredictionResult>(TOP_PATH);
    final sb = StringBuffer();
    final bd = typed_data.ByteData(4);
    for (var i = 0; i < TOP_PATH; i++) {
      final startOffset = (i * SEGMENT_SIZE) + TOP_PATH;
      final endOffset = startOffset + SEGMENT_SIZE;
      sb.clear();
      for (var j = startOffset; j < endOffset && j < predictionOutput.length; j++) {
        if (predictionOutput[j] == -65536) {
          break;
        }
        if (predictionOutput[j] >= 2) {
          sb.write(CHARSET[predictionOutput[j]]);
        }
      }
      bd.setUint32(0, predictionOutput[i]);
      result[i] = HTRWordPredictionResult(bd.getFloat32(0), sb.toString());
    }
    return result;
  }
}

class FlorResHTRPredictionOutputTransformer implements HTRPredictionOutputTransformer {
  static const CHARSET = "¶¤0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!,. ";
  static const String PAD_TK = "¶";
  static const String UNKNOWN_TK = "¤";

  List<HTRWordPredictionResult> transform(List<int> predictionOutput) {
    const TOP_PATH = 1;
    const SEGMENT_SIZE = 128;
    
    final result = List<HTRWordPredictionResult>(TOP_PATH);
    final sb = StringBuffer();
    final bd = typed_data.ByteData(4);
    for (var i = 0; i < TOP_PATH; i++) {
      final startOffset = (i * SEGMENT_SIZE) + TOP_PATH;
      final endOffset = startOffset + SEGMENT_SIZE;
      sb.clear();
      for (var j = startOffset; j < endOffset && j < predictionOutput.length; j++) {
        if (predictionOutput[j] == -65536) {
          break;
        }
        if (predictionOutput[j] >= 2) {
          sb.write(CHARSET[predictionOutput[j]]);
        }
      }
      bd.setUint32(0, predictionOutput[i]);
      result[i] = HTRWordPredictionResult(bd.getFloat32(0), sb.toString());
    }
    return result;
  }
}

class FlorResHTRApostrophePredictionOutputTransformer implements HTRPredictionOutputTransformer {
  static const CHARSET = "¶¤0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ?!,.'-";
  static const String PAD_TK = "¶";
  static const String UNKNOWN_TK = "¤";

  List<HTRWordPredictionResult> transform(List<int> predictionOutput) {
    const TOP_PATH = 1;
    const SEGMENT_SIZE = 128;
    
    final result = List<HTRWordPredictionResult>(TOP_PATH);
    final sb = StringBuffer();
    final bd = typed_data.ByteData(4);
    for (var i = 0; i < TOP_PATH; i++) {
      final startOffset = (i * SEGMENT_SIZE) + TOP_PATH;
      final endOffset = startOffset + SEGMENT_SIZE;
      sb.clear();
      for (var j = startOffset; j < endOffset && j < predictionOutput.length; j++) {
        if (predictionOutput[j] == -65536) {
          break;
        }
        if (predictionOutput[j] >= 2) {
          sb.write(CHARSET[predictionOutput[j]]);
        }
      }
      bd.setUint32(0, predictionOutput[i]);
      result[i] = HTRWordPredictionResult(bd.getFloat32(0), sb.toString());
    }
    return result;
  }
}