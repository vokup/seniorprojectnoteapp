class _InternalIterator<U, V> extends Iterator<V> {
  @override
  V get current => _current;

  V _current;
  final Iterator<U> iterator;
  final V Function (U) converter;

  _InternalIterator(this.iterator, this.converter);

  @override
  bool moveNext() {
    final smn = iterator.moveNext();
    if (smn) {
      _current = converter(iterator.current);
    }
    return smn;
  }
}

class IteratorToIterable<U, V> extends Iterable<V> {
  @override
  Iterator<V> get iterator => _InternalIterator(sourceIterator, converter);

  final Iterator<U> sourceIterator;
  final V Function (U) converter;

  IteratorToIterable(this.sourceIterator, this.converter);
}