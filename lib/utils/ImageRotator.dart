import 'dart:math' as math;
import 'dart:ui';

class ImageRotator {
  static double _toRadian(int degree) {
    return (math.pi / 180) * degree;
  }

  static Future<Image> rotateLeft(Image image) {
    final pr = PictureRecorder();
    final canvas = Canvas(pr);

//    canvas.drawRect(
//      Rect.fromLTWH(0, 0, image.height.toDouble(), image.width.toDouble()),
//      Paint()
//        ..color = prefix0.Colors.red
//        ..style = PaintingStyle.fill
//    );
//
//    canvas.rotate(_toRadian(-90));
////    canvas.translate(500, 1000);
//    canvas.drawRect(
//        Rect.fromLTWH(-image.width.toDouble() / 2, 0, 200, 200),
//        Paint()
//          ..color = prefix0.Colors.blue
//          ..style = PaintingStyle.fill
//    );
    canvas.rotate(_toRadian(-90));
    canvas.drawImage(image, Offset(-image.width.toDouble(), 0), Paint());
    return pr.endRecording().toImage(image.height, image.width);
  }

  static Future<Image> rotateRight(Image image) {
    final pr = PictureRecorder();
    final canvas = Canvas(pr);
    canvas.rotate(_toRadian(90));
    canvas.drawImage(image, Offset(0, -image.height.toDouble()), Paint());
    return pr.endRecording().toImage(image.height, image.width);
  }
}