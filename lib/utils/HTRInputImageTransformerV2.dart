import 'dart:math' as math;
import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class HTRInputImageTransformerV2 {
  static double _toRadian(int degree) {
    return (math.pi / 180) * degree;
  }
  
  Future<ui.Image> transform(ui.Image wordImage) async {
    const TARGET_WIDTH = 1024;
    const TARGET_HEIGHT = 128;

    final scale = math.max(
        wordImage.width / TARGET_WIDTH,
        wordImage.height / TARGET_HEIGHT
    );

    final scaledWidth = math.max(math.min(TARGET_WIDTH, wordImage.width / scale), 1).toDouble();
    final scaledHeight = math.max(math.min(TARGET_HEIGHT, wordImage.height / scale), 1).toDouble();

    final pr = ui.PictureRecorder();
    final canvas = ui.Canvas(pr);

    canvas.scale(-1, 1);
    canvas.rotate(_toRadian(90));

    canvas.drawRect(
      Rect.fromLTWH(0.0, 0.0, TARGET_WIDTH.toDouble(), TARGET_HEIGHT.toDouble()),
      Paint()
        ..style = PaintingStyle.fill
        ..color = Colors.white
    );

    canvas.drawImageRect(
      wordImage, 
      ui.Rect.fromLTWH(0.0, 0.0, wordImage.width.toDouble(), wordImage.height.toDouble()),
      ui.Rect.fromLTWH(0.0, 0.0, scaledWidth, scaledHeight),
      Paint()
    );

    final resultImage = await pr.endRecording().toImage(TARGET_HEIGHT, TARGET_WIDTH);
    return resultImage;
  }
}