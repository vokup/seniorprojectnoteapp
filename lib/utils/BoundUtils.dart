import 'dart:ui';

Rect mergeBound(Iterable<Rect> rects) {
  Rect result = rects.first;
  for (final r in rects.skip(1)) {
    result = result.expandToInclude(r);
  }
  return result;
}