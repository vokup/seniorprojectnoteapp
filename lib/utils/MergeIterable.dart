class MergeIterable<T> extends Iterable<T> {
  final List<Iterable<T>> iterables;

  MergeIterable(List<Iterable<T>> iterables):
    iterables = iterables;

  @override
  Iterator<T> get iterator => MergeIterator<T>(iterables.map((iterable) => iterable.iterator).toList());
}

class MergeIterator<T> extends Iterator<T> {
  @override
  T get current => _current;

  final List<Iterator<T>> iterators;

  T _current;
  Iterator<T> _cur;
  int _index = 0;

  MergeIterator(List<Iterator<T>> iterators): 
    iterators = iterators,
    _cur = iterators[0];

  @override
  bool moveNext() {
    var mv = _cur.moveNext();
    if (!!!mv && _index < iterators.length - 1) {
      _cur = iterators[++_index];
      mv = _cur.moveNext();
    }

    if (mv) {
      _current = _cur.current;
    }
    return mv;
  }
}