import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:senior_project_note_app/data/ConvertedTextRepository.dart';
import 'package:senior_project_note_app/data/DrawableGroupRepository.dart';
import 'package:senior_project_note_app/libs/DrawingAreaScaler.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingControlPanel/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/StaticFile/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/containers/DrawingPageAppLayout.dart';
import 'package:senior_project_note_app/pages/Global/bloc/bloc.dart';
import 'package:senior_project_note_app/pages/Global/bloc/state.dart';
import 'package:senior_project_note_app/services/DrawableIDProviderService.dart';
import 'package:senior_project_note_app/services/GroupQueueService.dart';
import 'package:senior_project_note_app/services/WordGroupingService.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([ 
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight
    ]);

    final dgp = DrawableGroupRepository();
    final ctr = ConvertedTextRepository();
    
    final areaScaler = AreaScaler(3508.0, 2480.0);
    final gqs = GroupQueueService(5000);

    final didps = DrawableIDProviderService();
    final wgs = WordGroupingService(dgp);

    final gb = GlobalBloc();
    final dtb = DrawingToolbarBloc();  
    final dpb = DrawingPropertiesBloc(dtb);
    final dab = DrawingAreaBloc(dpb, gb, didps, wgs, dgp, ctr, gqs, areaScaler);
    dtb.listenToDrawingAreaBloc(dab);

    return MultiBlocProvider(
      providers: [
        BlocProvider<StaticFileBloc>(create: (context) => StaticFileBloc(dab, gb, dtb, ctr, didps)),
        BlocProvider<DrawingPropertiesBloc>(create: (context) => dpb),
        BlocProvider<DrawingToolbarBloc>(create: (context) => dtb),
        BlocProvider<DrawingControlPanelBloc>(create: (context) => DrawingControlPanelBloc(dpb)),
        BlocProvider<DrawingAreaBloc>(create: (context) => dab),
        BlocProvider<GlobalBloc>(create: (context) => gb)
      ],
      child: MultiRepositoryProvider(
        providers: [
          RepositoryProvider<AreaScaler>(create: (context) => areaScaler),
        ], 
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(
            primarySwatch: Colors.green,
          ),
          home: BlocListener<GlobalBloc, GlobalState>(
            listener: (context, state) {
              print("GlobalBloc: ${state.isProgressDialogShowing}");
              if (state.isProgressDialogShowing) {
                showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return WillPopScope(
                      child: Center(
                        child: FractionallySizedBox(
                          widthFactor: 0.25,
                          heightFactor: 0.25,
                          child: Scaffold(
                            body: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  CircularProgressIndicator(value: null),
                                  SizedBox(height: 16),
                                  Text(state.progressDialogMessage)
                                ],
                              ),
                            )
                          )
                        )
                      ), 
                      onWillPop: () async {
                        return false;
                      }
                    );
                  }
                );
              } else {
                Navigator.of(context).pop();
              }
            },
            child: DrawingPageAppLayout()
          )
        )
      )
    );
  }
}