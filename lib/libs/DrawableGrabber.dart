import 'dart:ui';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/libs/LinkedListExt.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';

class _DummyDrawable extends Drawable {
  _DummyDrawable(int id) : super(id);
  @override
  void draw(Canvas canvas, Size size) => throw UnimplementedError();
}

abstract class DrawableGrabberContract {
  Drawable lookupByID(int id);
}

class DrawingAreaBlocDrawableGrabber implements DrawableGrabberContract {

  final Set<DrawableEntry> _internalSet;

  DrawingAreaBlocDrawableGrabber(Set<DrawableEntry> drawables):
    _internalSet = drawables;

  @override
  Drawable lookupByID(int id) {
    final d = _internalSet.lookup(DrawableEntry(_DummyDrawable(id)));
    return d != null ? d.drawable : null;
  }
}