import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/services.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/drawables/ImageDrawable.dart';
import 'package:senior_project_note_app/drawables/PenDrawable.dart';
import 'package:senior_project_note_app/drawables/TextDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableSerializerContract.dart';
import 'package:senior_project_note_app/libs/serialization/ImageDrawableSerializer.dart';
import 'package:senior_project_note_app/libs/serialization/PenDrawableSerializer.dart';
import 'package:senior_project_note_app/libs/serialization/TextDrawableSerializer.dart';

abstract class StaticFileWriterContract {
  void save(String absolutePath);
}

/**
 * SAVING STEP:
 * 1. Get latest drawable id
 * 2. Save group drawable by type
 * 3. For image drawable using gzip for raw image data compression/decompression
 * [ Structure ]
 * {
 *    "lastest_drawable_id": 0,
 *    "penDrawables": []
 *    
 *    
 * }
 */

class StaticFileWriter extends StaticFileWriterContract {
  final Iterable<Drawable> _toSaveDrawables;
  final int _latestDrawableId;

  StaticFileWriter(this._toSaveDrawables, this._latestDrawableId);

  Future<List<int>> saveAsBytes() async {
    final penDrawables = List<String>();
    final imageDrawables = List<String>();
    final textDrawables = List<String>();

    for (final d in _toSaveDrawables) {
      DrawableJsonSerializerContract serializer;
      List<String> selectedList;

      if (d is PenDrawable) {
        serializer = PenDrawableSerializer(drawable: d);
        selectedList = penDrawables;
      } else if (d is ImageDrawable) {
        serializer = ImageDrawableSerializer(drawable: d);  
        selectedList = imageDrawables;
      } else if (d is TextDrawable) {
        serializer = TextDrawableSerializer(drawable: d);
        selectedList = textDrawables;
      }
      final str = await serializer.jsonSerialize();
      selectedList.add(str);
    }

    final savedContent = jsonEncode(<String, dynamic> {
      "latestDrawableId": _latestDrawableId,
      "penDrawables": penDrawables,
      "textDrawables": textDrawables,
      "imageDrawables": imageDrawables
    });

    return gzip.encode(utf8.encode(savedContent));
  }

  @override
  Future<void> save(String absolutePath) async {
    final savedContent = await saveAsBytes();
    final file = File(absolutePath);
    await file.writeAsBytes(savedContent, flush: true);
  }
}