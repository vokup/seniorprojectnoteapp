import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/PenDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableConverterContract.dart';

class PenDrawableJsonConverter extends DrawableJsonConverterContract<PenDrawable> {
  @override
  Future<PenDrawable> convert(Map<String, dynamic> json) async {
    final drawableId = json["id"];
    final isNormalPen = json["isNormalPen"];

    final bd = ByteData(16);
    bd.setUint64(0, json["start"][0]);
    final sx = bd.getFloat64(0);
    bd.setUint64(0, json["start"][1]);
    final sy = bd.getFloat64(0);
    final red = json["color"][0];
    final green = json["color"][1];
    final blue = json["color"][2];
    bd.setUint64(0, json["thickness"]);
    final thickness = bd.getFloat64(0);

    final List<Point<double>> points = List<Point<double>>();
    for (final p in json["points"]) {
      bd.setUint64(0, p[0]);
      final x = bd.getFloat64(0);
      bd.setUint64(0, p[1]);
      final y = bd.getFloat64(0);
      points.add(Point(x, y));
    }

    return PenDrawable.fromPoints(
      id: drawableId,
      isNormalPen: isNormalPen,
      sx: sx,
      sy: sy,
      color: Color.fromARGB(255, red, green, blue),
      thickness: thickness,
      points: points
    );
  }
}