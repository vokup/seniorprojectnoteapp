abstract class DrawableJsonConverterContract<T> {
  Future<T> convert(Map<String, dynamic> json);
}