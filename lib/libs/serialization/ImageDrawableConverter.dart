import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/ImageDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableConverterContract.dart';

class ImageDrawableJsonConverter extends DrawableJsonConverterContract<ImageDrawable> {
  @override
  Future<ImageDrawable> convert(Map<String, dynamic> json) async {
    final drawableId = json["id"];

    final bd = ByteData(16);
    bd.setUint64(0, json["offset"][0]);
    final x = bd.getFloat64(0);
    bd.setUint64(0, json["offset"][1]);
    final y = bd.getFloat64(0);
    bd.setUint64(0, json["size"][0]);
    final width = bd.getFloat64(0);
    bd.setUint64(0, json["size"][1]);
    final height = bd.getFloat64(0);

    final imageData = base64Decode(json["imageData"]);

    final imageCodec = await instantiateImageCodec(imageData);
    final imageFrame = await imageCodec.getNextFrame();
    final image = imageFrame.image;
    
    return ImageDrawable(
      drawableId: drawableId,
      image: image,
      offset: Offset(x, y),
      size: Size(width, height),
      showControlBox: false
    );
  }
}