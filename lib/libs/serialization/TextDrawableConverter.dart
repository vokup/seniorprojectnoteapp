import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:senior_project_note_app/drawables/TextDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableConverterContract.dart';

class TextDrawableJsonConverter extends DrawableJsonConverterContract<TextDrawable> {
  @override
  Future<TextDrawable> convert(Map<String, dynamic> json) async {
    final drawableId = json["id"];
    final text = json["text"];

    final bd = ByteData(16);
    bd.setUint64(0, json["size"]);
    final fontSize = bd.getFloat64(0);
    bd.setUint64(0, json["offset"][0]);
    final offsetX = bd.getFloat64(0);
    bd.setUint64(0, json["offset"][1]);
    final offsetY = bd.getFloat64(0);

    bd.setUint64(0, json["renderedBound"][0]);
    final renderBoundX = bd.getFloat64(0);
    bd.setUint64(0, json["renderedBound"][1]);
    final renderBoundY = bd.getFloat64(0);
    bd.setUint64(0, json["renderedBound"][2]);
    final renderBoundWidth = bd.getFloat64(0);
    bd.setUint64(0, json["renderedBound"][3]);
    final renderBoundHeight = bd.getFloat64(0);

    final colorJson = json["color"];
    final color = Color.fromARGB(255, colorJson[0], colorJson[1], colorJson[2]);

    return TextDrawable(
      drawableId,
      text: text,
      fontSize: fontSize,
      offset: Offset(offsetX, offsetY),
      renderedBound: Rect.fromLTWH(renderBoundX, renderBoundY, renderBoundWidth, renderBoundHeight),
      color: color,
      groupId: -1
    );
  }
}