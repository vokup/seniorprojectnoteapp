abstract class DrawableJsonSerializerContract {
  Future<String> jsonSerialize();
}