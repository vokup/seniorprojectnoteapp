import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/TextDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableSerializerContract.dart';

class TextDrawableSerializer extends DrawableJsonSerializerContract {

  final TextDrawable drawable;

  TextDrawableSerializer({
    @required this.drawable
  });

  Map<String, dynamic> toJson() {
    final bd = ByteData(16);
    bd.setFloat64(0, drawable.fontSize);
    final fontSize = bd.getUint64(0);

    bd.setFloat64(0, drawable.offset.dx);
    final offsetX = bd.getUint64(0);
    bd.setFloat64(0, drawable.offset.dy);
    final offsetY = bd.getUint64(0);

    bd.setFloat64(0, drawable.getBound().topLeft.dx);
    final renderBoundX = bd.getUint64(0);
    bd.setFloat64(0, drawable.getBound().topLeft.dy);
    final renderBoundY = bd.getUint64(0);
    bd.setFloat64(0, drawable.getBound().width);
    final renderBoundWidth = bd.getUint64(0);
    bd.setFloat64(0, drawable.getBound().height);
    final renderBoundHeight = bd.getUint64(0);

    return <String, dynamic> {
      "type": "TextDrawable",
      "id": drawable.id,
      "text": drawable.text,
      "size": fontSize,
      "color": [ drawable.color.red, drawable.color.green, drawable.color.blue ],
      "offset": [ offsetX, offsetY ],
      "renderedBound": [ renderBoundX, renderBoundY, renderBoundWidth, renderBoundHeight ], 
    };
  }

  @override
  Future<String> jsonSerialize() async {
    return jsonEncode(this);
  }
}