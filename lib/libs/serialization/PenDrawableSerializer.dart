import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/PenDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableSerializerContract.dart';

class PenDrawableSerializer extends DrawableJsonSerializerContract {
  final PenDrawable drawable;

  PenDrawableSerializer({
    @required this.drawable
  });

  Map<String, dynamic> toJson() {
    final bd = ByteData(16);
    bd.setFloat64(0, drawable.sx);
    final sx = bd.getUint64(0);
    bd.setFloat64(0, drawable.sy);
    final sy = bd.getUint64(0);
    bd.setFloat64(0, drawable.paint.strokeWidth);
    final thickness = bd.getUint64(0);

    return <String, dynamic> {
      "type": "PenDrawable",
      "id": drawable.id,
      "isNormalPen": drawable.isNormalPen,
      "start": [ sx, sy ],
      "color": [ drawable.paint.color.red, drawable.paint.color.green, drawable.paint.color.blue ],
      "thickness": thickness,
      "points": drawable.points.map((point) {
        bd.setFloat64(0, point.x);
        final x = bd.getUint64(0);
        bd.setFloat64(0, point.y);
        final y = bd.getUint64(0);
        return [ x, y ];
      }).toList()
    };
  }

  @override
  Future<String> jsonSerialize() async {
    return jsonEncode(this);
  }
}