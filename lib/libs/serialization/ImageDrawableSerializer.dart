import 'dart:convert';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/ImageDrawable.dart';
import 'package:senior_project_note_app/libs/serialization/DrawableSerializerContract.dart';

class ImageDrawableSerializer extends DrawableJsonSerializerContract {

  final ImageDrawable drawable;
  
  String _imageDataBase64;

  ImageDrawableSerializer({
    @required this.drawable
  });

  Map<String, dynamic> toJson() {
    final bd = ByteData(16);
    bd.setFloat64(0, drawable.offset.dx);
    final x = bd.getUint64(0);
    bd.setFloat64(0, drawable.offset.dy);
    final y = bd.getUint64(0);
    bd.setFloat64(0, drawable.size.width);
    final width = bd.getUint64(0);
    bd.setFloat64(0, drawable.size.height);
    final height = bd.getUint64(0);
    
    return <String, dynamic> {
      "type": "ImageDrawable",
      "id": drawable.id,
      "offset": [ x, y ],
      "size": [ width, height ],
      "imageData": _imageDataBase64
    };
  }

  @override
  Future<String> jsonSerialize() async {
    final imageBytes = await drawable.image.toByteData(format: ImageByteFormat.png);
    _imageDataBase64 = base64Encode(imageBytes.buffer.asUint8List());
    return jsonEncode(this);
  }
}