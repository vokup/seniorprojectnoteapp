class AreaScaler {
  final double _targetWidth;
  final double _targetHeight;
  double _workingAreaWidth;
  double _workingAreaHeight;

  double get targetWidth => _targetWidth;
  double get targetHeight => _targetHeight;
  double get workingAreaWidth => _workingAreaWidth;
  double get workingAreaHeight => _workingAreaHeight;
  double get calculatedScaleWidth => _calculatedScaleWidth;
  double get calculatedScaleHeight => _calculatedScaleHeight;

  double _calculatedScaleWidth;
  double _calculatedScaleHeight;

  AreaScaler(
    double targetWidth, 
    double targetHeight,
    [
      double workingAreaWidth = 1.0,
      double workingAreaHeight = 1.0 
    ]
  ):
  _targetWidth = targetWidth,
  _targetHeight = targetHeight,
  _workingAreaWidth = workingAreaWidth,
  _workingAreaHeight = workingAreaHeight,
  _calculatedScaleWidth = targetWidth / workingAreaWidth,
  _calculatedScaleHeight = targetHeight / workingAreaHeight;

  bool isSameWorkingAreaSize(double width, double height) {
    return _workingAreaWidth == width && _workingAreaHeight == height;
  }

  void setWorkingAreaWidth(double width) {
    _workingAreaWidth = width;
    _calculatedScaleWidth = _targetWidth / _workingAreaWidth;
  }

  void setWorkingAreaHeight(double height) {
    _workingAreaHeight = height;
    _calculatedScaleHeight = _targetHeight / _workingAreaHeight;
  }

  void setWorkingAreaSize(double width, double height) {
    setWorkingAreaWidth(width);
    setWorkingAreaHeight(height);
  }

  double getTargetX(double workingAreaX) {
    return workingAreaX * _calculatedScaleWidth;
  }

  double getTargetY(double workingAreaY) {
    return workingAreaY * _calculatedScaleHeight;
  }
}