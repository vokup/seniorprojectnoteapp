import 'dart:collection';

import 'package:senior_project_note_app/drawables/Drawable.dart';

class LinkedListExt<E extends LinkedListEntry<E>> {
  final LinkedList<E> list;

  LinkedListExt(this.list);

  LinkedListExt<E> copyWith(void Function (LinkedList<E>) executor) {
    executor(this.list);
    return LinkedListExt(this.list);
  }

  factory LinkedListExt.empty() {
    return LinkedListExt(LinkedList<E>());
  }
}

class DrawableEntry extends LinkedListEntry<DrawableEntry> {
  final Drawable drawable;
  DrawableEntry(this.drawable);
}