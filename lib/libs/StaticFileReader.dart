import 'dart:convert';
import 'dart:io';

import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/libs/serialization/ImageDrawableConverter.dart';
import 'package:senior_project_note_app/libs/serialization/PenDrawableConverter.dart';
import 'package:senior_project_note_app/libs/serialization/TextDrawableConverter.dart';

class SavedDataModel {
  final int latestDrawableId;
  final Iterable<Drawable> penDrawables;
  final Iterable<Drawable> textDrawables;
  final Iterable<Drawable> imageDrawables;

  SavedDataModel(
    this.latestDrawableId,
    this.penDrawables,
    this.textDrawables,
    this.imageDrawables
  );
}

class StaticFileReader {
  Future<SavedDataModel> read(String absolutePath) async {
    final file = File(absolutePath);
    final data = await file.readAsBytes();
    final jsonString = utf8.decode(gzip.decode(data));

    final json = jsonDecode(jsonString);

    final penDrawables = List<Drawable>();
    final textDrawables = List<Drawable>();
    final imageDrawables = List<Drawable>();

    for (final d in json["penDrawables"]) {
      final dJson = jsonDecode(d);
      final converter = PenDrawableJsonConverter();
      final convertedDrawable = await converter.convert(dJson);
      penDrawables.add(convertedDrawable);
    }

    for (final d in json["imageDrawables"]) {
      final dJson = jsonDecode(d);
      final converter = ImageDrawableJsonConverter();
      final convertedDrawable = await converter.convert(dJson);
      penDrawables.add(convertedDrawable);
    }

    for (final d in json["textDrawables"]) {
      final dJson = jsonDecode(d);
      final converter = TextDrawableJsonConverter();
      final convertedDrawable = await converter.convert(dJson);
      penDrawables.add(convertedDrawable);
    }

    return SavedDataModel(
      json["latestDrawableId"],
      penDrawables,
      textDrawables,
      imageDrawables
    );
  }
}