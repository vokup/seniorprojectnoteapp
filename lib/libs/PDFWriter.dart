import 'dart:typed_data';
import 'dart:ui';
import 'dart:io' as io;
import 'package:flutter/material.dart';
import 'package:image/image.dart' as imagelib;
import 'package:pdf/pdf.dart';
import 'package:flutter/services.dart' as service;

class PDFWriter {
  PdfDocument _document;
  PdfPage _page;
  PdfGraphics _graphics;
  PdfFont _font;

  final double _externalWidth;
  final double _externalHeight;

  PDFWriter([ double externalWidth = 3508.0, double externalHeight = 2480.0 ])
  :
  _externalWidth = externalWidth,
  _externalHeight = externalHeight;

  Future<void> intialize() async {
    _document = PdfDocument();
    _page = PdfPage(_document, pageFormat: PdfPageFormat.a4.landscape);
    _graphics = _page.getGraphics();

    final fontBytes = await service.rootBundle.load("assets/fonts/OpenSansCondensed-Light.ttf");
    _font = PdfTtfFont(_document, fontBytes);
  }

  Offset _translateExternalCoordinate(Offset externalOffset) {
    return externalOffset.scale(
      _page.pageFormat.width / _externalWidth, 
      _page.pageFormat.height / _externalHeight
    );
  }

  // remap (0,0) to top-left from bottom-left
  Offset _translateCoordinate(Offset desireOffset) {
    final engineOffset = Offset(
      desireOffset.dx, 
      _page.pageFormat.height - desireOffset.dy
    );
    return engineOffset;
  }

  void _addEngineLine(Offset start, Offset end, double thickness, [ Color color = Colors.black ])
  {
    _graphics.setLineWidth(thickness);
    _graphics.moveTo(start.dx, start.dy);
    _graphics.lineTo(end.dx, end.dy);
    _graphics.setColor(PdfColor.fromInt(color.value));
    _graphics.strokePath();
  }

  void addLine(Offset start, Offset end, double thickness, [ Color color = Colors.black ]) {
    start = _translateCoordinate(start);
    end = _translateCoordinate(end);
    _addEngineLine(start, end, thickness);
  }

  // add path from points which are relative to top-left
  void addPath(Iterable<Offset> points, [ double thickness = 1.0, Color color = Colors.black ]) {
    _graphics.setStrokeColor(PdfColor.fromInt(color.value));
    _graphics.setLineWidth(thickness);
    final firstPoint = _translateCoordinate(_translateExternalCoordinate(points.first));
    _graphics.moveTo(firstPoint.dx, firstPoint.dy);
    for (final p in points.skip(1)) {
      final tp = _translateCoordinate(_translateExternalCoordinate(p));
      _graphics.lineTo(tp.dx, tp.dy);
    }
    _graphics.strokePath();
  }

  void addImage(Uint8List jpegBytes, Offset offset, Offset size) {
    offset = _translateCoordinate(_translateExternalCoordinate(offset));
    size = _translateExternalCoordinate(size);

    final srcImage = imagelib.decodeImage(jpegBytes);
    final finalImage = imagelib.copyResize(srcImage, width: size.dx.toInt(), height: size.dy.toInt());

    _graphics.drawImage(
      PdfImage(
        _document,
        image: finalImage.data.buffer.asUint8List(),
        width: finalImage.width,
        height: finalImage.height
      ),
      offset.dx, 
      offset.dy - finalImage.height
    );
  }

  void _addEngineString(String text, Offset baselineOffset, double fontSize, [ Color color = Colors.black ]) {
    _graphics.setColor(PdfColor.fromInt(color.value));
    _graphics.drawString(
      _font, 
      fontSize,
      text, 
      baselineOffset.dx,
      baselineOffset.dy
    );
  }

  void addString(String text, Offset topLeftOffset, double extHeight, [ Color color = Colors.black ]) {
    topLeftOffset = _translateExternalCoordinate(topLeftOffset);

    final targetHeight = extHeight * (_page.pageFormat.height / _externalHeight);
    // _stringDebug(text, offset, fontSize);
    final metrics = _font.stringMetrics(text);
    final engineFontSize = targetHeight / metrics.height;

    print("HEY1: ${metrics.leftBearing}");
    print("[$text]2: ${targetHeight * metrics.top}, $engineFontSize, $targetHeight");
    // this line represent baseline
    // _addEngineLine(engineDrawingOffset, engineDrawingOffset.translate(1000.0, 0), 5.0);

    final engineOffset = _translateCoordinate(topLeftOffset).translate(metrics.leftBearing * engineFontSize, -targetHeight - targetHeight * metrics.top);

    // _graphics.setColor(PdfColor.fromInt(Colors.red.value));
    // _graphics.setLineWidth(1.0);
    // _graphics.drawRect(
    //   engineOffset.dx,
    //   engineOffset.dy + targetHeight * metrics.top,
    //   metrics.width * engineFontSize,
    //   targetHeight
    // );
    // _graphics.strokePath();

    _addEngineString(text, engineOffset, engineFontSize, color);
  }

  List<int> saveAsBytes() {
    return _document.save();
  }

  void save(String filePath) {
    final file = io.File(filePath);
    final pdfBytes = saveAsBytes();
    print("PDFBytes: ${pdfBytes.length}");
    file.writeAsBytesSync(pdfBytes);
  }
}