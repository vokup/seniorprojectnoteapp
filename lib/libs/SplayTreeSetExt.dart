import 'dart:collection';

class SplayTreeSetExt<E> {
  final SplayTreeSet<E> set;

  SplayTreeSetExt(
    this.set
  );

  SplayTreeSetExt<E> copyWith(void Function (Set<E>) executor) {
    executor(this.set);
    return SplayTreeSetExt(this.set);
  }

  factory SplayTreeSetExt.empty([ int compare(E key1, E key2)]) {
    return SplayTreeSetExt(SplayTreeSet<E>(compare));
  }
}