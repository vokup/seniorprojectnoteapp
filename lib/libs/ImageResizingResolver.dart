import 'dart:math';
import 'dart:ui';

import 'package:senior_project_note_app/drawables/ImageDrawable.dart';
import 'package:senior_project_note_app/utils/GeometryCalculation.dart';

enum ResizingPosition {
  // resizing is not happen
  None,

  Top,
  Left,
  Right,
  Bottom,

  // must keep aspect-ratio
  TopLeft,
  TopRight,
  BottomLeft,
  BottomRight
}

class ImageDrawableResizingResolver {
  static ResizingPosition resolvePosition(ImageDrawable drawable, Point<double> actionPoint) {
    final tl = drawable.getBound().topLeft;
    final br = drawable.getBound().bottomRight;
    final height = drawable.getBound().height;
    final halfHeight = height / 2;
    
    final List<Rect> resizingRects = [
      Rect.fromCircle(center: drawable.getBound().topLeft, radius: ImageDrawable.RESIZING_SIZE * 4),
      Rect.fromCircle(center: drawable.getBound().topCenter, radius: ImageDrawable.RESIZING_SIZE * 4),
      Rect.fromCircle(center: drawable.getBound().topRight, radius: ImageDrawable.RESIZING_SIZE * 4),

      Rect.fromCircle(center: Offset(tl.dx, tl.dy + halfHeight), radius: ImageDrawable.RESIZING_SIZE * 4),
      Rect.fromCircle(center: Offset(br.dx, tl.dy + halfHeight), radius: ImageDrawable.RESIZING_SIZE * 4),

      Rect.fromCircle(center: drawable.getBound().bottomLeft, radius: ImageDrawable.RESIZING_SIZE * 4),
      Rect.fromCircle(center: drawable.getBound().bottomCenter, radius: ImageDrawable.RESIZING_SIZE * 4),
      Rect.fromCircle(center: drawable.getBound().bottomRight, radius: ImageDrawable.RESIZING_SIZE * 4),
    ];

    const RESULTS = [
      ResizingPosition.TopLeft,
      ResizingPosition.Top,
      ResizingPosition.TopRight,
      ResizingPosition.Left,
      ResizingPosition.Right,
      ResizingPosition.BottomLeft,
      ResizingPosition.Bottom,
      ResizingPosition.BottomRight
    ];

    for (var i = 0; i < resizingRects.length; i++) {
      if (GeometryCalculation.isInBound(resizingRects[i], actionPoint)) {
        return RESULTS[i];
      }
    }
    return ResizingPosition.None;
  }
}