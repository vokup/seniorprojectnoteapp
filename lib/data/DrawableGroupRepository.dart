import 'dart:ui';
import 'dart:collection';

import 'package:senior_project_note_app/base/Resetable.dart';

class DrawableGroupModel {
  final int id;
  final Rect bound;
  final Set<int> drawableIds;

  DrawableGroupModel(
      this.id,
      this.bound,
      this.drawableIds
  );
}

class DrawableGroupRepository implements Resetable {
  Iterable<DrawableGroupModel> get groups => _groups;
  final Set<DrawableGroupModel> _groups;

  DrawableGroupRepository():
    _groups = SplayTreeSet<DrawableGroupModel>((k1, k2) => k2.id - k1.id);

  DrawableGroupModel lookup(int id) {
    final g = _groups.lookup(DrawableGroupModel(id, null, null));
    return g;
  }

  void add(DrawableGroupModel model) {
    _groups.add(model);
  }

  void removeByID(int id) {
    _groups.remove(DrawableGroupModel(id, null, null));
  }

  @override
  Future<void> reset() async {
    _groups.clear();
  }
}