import 'dart:collection';
import "dart:ui";
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/base/Resetable.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';

class ConvertedTextModel {
  final int id;
  final Iterable<Drawable> drawables;
  final String text;
  final Offset offset;
  final Offset baseline;
  final double fontSize;
  final Rect textBound;
  final Color color;

  ConvertedTextModel({
    @required this.id,
    @required this.drawables,
    @required this.text, 
    @required this.offset, 
    @required this.baseline, 
    @required this.fontSize, 
    @required this.textBound,
    @required this.color
  });
}

class ConvertedTextRepository implements Resetable {
  final Set<ConvertedTextModel> _convertedTexts;

  Iterable<ConvertedTextModel> get convertedTexts => _convertedTexts;

  ConvertedTextRepository():
    _convertedTexts = SplayTreeSet<ConvertedTextModel>((k1, k2) => k2.id - k1.id);

  ConvertedTextModel lookup(int id) {
    final ct = _convertedTexts.lookup(ConvertedTextModel(id: id));
    return ct;
  }

  void add(ConvertedTextModel model) {
    _convertedTexts.add(model);
  }

  void removeByID(int id) {
    _convertedTexts.remove(ConvertedTextModel(id: id));
  }

  @override
  Future<void> reset() async {
    _convertedTexts.clear();
  }
}