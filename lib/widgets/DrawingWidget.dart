import 'package:flutter/material.dart';

class DrawingWidget extends StatelessWidget {
  final Widget customPaint;

  DrawingWidget({ @required this.customPaint });

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Color.fromARGB(255, 0xcc, 0xcc, 0xcc),
      width: double.infinity,
      height: double.infinity,
      child: customPaint
    );
  }
}