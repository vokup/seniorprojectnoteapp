import 'package:flutter/material.dart';

class AppLayout extends StatelessWidget {
  final Widget titleWidget;
  final List<Widget> actions;
  final Widget child;

  AppLayout({ this.titleWidget, this.actions, this.child });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: this.titleWidget,
        actions: this.actions
      ),
      body: Center(
        child: this.child
      ),
    );
  }
}