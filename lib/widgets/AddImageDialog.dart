import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class _AddImageDialogState extends State<AddImageDialog> {

  final urlField = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Text("Add Image"),
      contentPadding: EdgeInsets.all(16.0),
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width * 0.4,
          child: Row(
            children: <Widget>[
              Text("Image URL: "),
              Expanded(
                child: TextField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder()
                  ),
                  controller: urlField,
                  maxLines: 1,
                )
              ),
              SizedBox(width: 8),
              RaisedButton(
                color: Colors.green,
                textColor: Colors.white,
                child: Text("ADD IMAGE"),
                onPressed: () {
                  if (urlField.text.trim().length > 0) {
                    widget.onAddImageFromUrlClick(urlField.text);
                  }
                }
              )
            ]
          )
        ),
        SizedBox(height: 12),
        Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              color: Colors.green,
              textColor: Colors.white,
              child: Text("SELECT FROM DEVICE"),
              onPressed: () async {
                final path = await FilePicker.getFilePath(type: FileType.IMAGE);
                if (path != null && path != "") {
                  widget.onSelectFromDeviceButtonClick(path);
                }
              },
            ),
          ]
        )
      ],
    );
  }
}

class AddImageDialog extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => _AddImageDialogState();

  final void Function(String) onSelectFromDeviceButtonClick;
  final void Function(String) onAddImageFromUrlClick;

  AddImageDialog({
    this.onSelectFromDeviceButtonClick,
    this.onAddImageFromUrlClick
  });
}