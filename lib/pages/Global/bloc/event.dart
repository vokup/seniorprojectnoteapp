import 'package:equatable/equatable.dart';

class GlobalEvent extends Equatable {
  @override
  List<Object> get props => _props;
  final List _props;

  GlobalEvent([List props = const []]):
        _props = props;
}

class ShowProgressDialog extends GlobalEvent {
  final String message;
  ShowProgressDialog(this.message): super([ message ]);
} 

class HideProgressDialog extends GlobalEvent { }