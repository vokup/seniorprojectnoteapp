import 'dart:io';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:senior_project_note_app/pages/Global/bloc/event.dart';
import 'package:senior_project_note_app/pages/Global/bloc/state.dart';

class GlobalBloc extends Bloc<GlobalEvent, GlobalState> {
  @override
  GlobalState get initialState => GlobalState.initial();

  @override
  Stream<GlobalState> mapEventToState(GlobalEvent event) async* {
    if (event is ShowProgressDialog) {
      yield* _handleShowProgressDialog(event);
    } else if (event is HideProgressDialog) {
      yield* _handleHideProgressDialog(event);
    }
  }

  Stream<GlobalState> _handleShowProgressDialog(ShowProgressDialog event) async* {
    yield state.copyWith(
      isProgressDialogShowing: true,
      progressDialogMessage: event.message
    );
  }

  Stream<GlobalState> _handleHideProgressDialog(HideProgressDialog event) async* {
    yield state.copyWith(
      isProgressDialogShowing: false,
      progressDialogMessage: ""
    );
  }
}