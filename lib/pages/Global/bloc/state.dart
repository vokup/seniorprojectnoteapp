import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
class GlobalState {
  final bool isProgressDialogShowing;
  final String progressDialogMessage;

  GlobalState({
    this.isProgressDialogShowing,
    this.progressDialogMessage
  });

  factory GlobalState.initial() {
    return GlobalState(
      isProgressDialogShowing: false,
      progressDialogMessage: ""
    );
  }

  GlobalState copyWith({
    bool isProgressDialogShowing,
    String progressDialogMessage
  }) {
    return GlobalState(
      isProgressDialogShowing: isProgressDialogShowing ?? this.isProgressDialogShowing,
      progressDialogMessage: progressDialogMessage ?? this.progressDialogMessage
    );
  }
}