import 'dart:math';
import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:senior_project_note_app/pages/DrawingPage/models/DrawingAreaPointerModel.dart';

class SingleTouchRecognizer extends OneSequenceGestureRecognizer {
  int _pointer = -1;
  bool isFirst = true;
  // callback
  void Function(PointerEvent) onDown;
  void Function(PointerEvent) onUp;
  void Function(PointerEvent) onMove;

  SingleTouchRecognizer({ PointerDeviceKind kind }): 
    super(kind: kind);

  @override
  String get debugDescription => "SingleTouch";

  void _onDown(PointerEvent event) {
    if (onDown != null) {
      onDown(event);
    }
  }

  void _onUp(PointerEvent event) {
    if (onUp != null) {
      onUp(event);
    }
  }

  void _onMove(PointerEvent event) {
    if (onMove != null) {
      onMove(event);
    }
  }

  @override 
  void addAllowedPointer(PointerDownEvent event) {
    if (_pointer == -1) {
      isFirst = true;
      _pointer = event.pointer;
      startTrackingPointer(event.pointer, event.transform);
      _onDown(event);
    }
  }

  @override
  void didStopTrackingLastPointer(int pointer) {
    // print("didStopTrackingLastPointer: $pointer");
  }

  @override
  void handleEvent(PointerEvent event) {
    if (event is PointerUpEvent || event is PointerCancelEvent) {
      stopTrackingPointer(event.pointer);
      _pointer = -1;
      _onUp(event);

    } else {
      if (event is PointerMoveEvent) {
        _onMove(event);
      }
    }
  }
}

class DrawingPageWidget extends StatelessWidget {

  final bool isControlPanelSittingLeft;

  final Point<double> controlPanelPosition;
  final void Function(DrawingAreaPointerModel) onDrawingAreaPointerDown;
  final void Function(DrawingAreaPointerUpModel) onDrawingAreaPointerUp;
  final void Function(DrawingAreaPointerModel) onDrawingAreaPointerMove;
  final void Function(DrawingAreaPointerLongPressModel) onDrawingAreaPointerLongPress;

  final Widget drawingArea;
  final Widget drawingSideControlPanel;

  DrawingPageWidget({
    this.isControlPanelSittingLeft = true,

    this.controlPanelPosition,
    this.onDrawingAreaPointerDown,
    this.onDrawingAreaPointerUp,
    this.onDrawingAreaPointerMove,
    this.onDrawingAreaPointerLongPress,

    this.drawingArea,
    this.drawingSideControlPanel
  });

  Widget drawingPane(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          color: Color.fromARGB(255, 0xcc, 0xcc, 0xcc),
          width: double.infinity,
          height: double.infinity,
        ),
        Padding(
          padding: EdgeInsets.all(8),
          child: Align(
            child: AspectRatio(
              aspectRatio: MediaQuery.of(context).orientation == Orientation.landscape ? 3508 / 2480 : 2480 / 3508,
              child: RawGestureDetector(
                gestures: <Type, GestureRecognizerFactory>{
                  SingleTouchRecognizer: GestureRecognizerFactoryWithHandlers<SingleTouchRecognizer>(
                    () => SingleTouchRecognizer(),
                    (instance) {
                      instance
                        ..onDown = (ev) {
                          this.onDrawingAreaPointerDown(
                              DrawingAreaPointerModel(
                                  x: ev.localPosition.dx,
                                  y: ev.localPosition.dy,
                                  dx: ev.localDelta.dx,
                                  dy: ev.localDelta.dy,
                                  kind: ev.kind,
                                  pointer: ev.pointer
                              )
                          );
                        }
                        ..onUp = (ev) {
                          this.onDrawingAreaPointerUp(
                              DrawingAreaPointerUpModel(
                                  x: ev.localPosition.dx,
                                  y: ev.localPosition.dy,
                                  dx: ev.localDelta.dx,
                                  dy: ev.localDelta.dy,
                                  kind: ev.kind,
                                  pointer: ev.pointer,
                                  pointerCount: ev.device
                              )
                          );
                        }
                        ..onMove = (ev) {
                          this.onDrawingAreaPointerMove(
                              DrawingAreaPointerModel(
                                  x: ev.localPosition.dx,
                                  y: ev.localPosition.dy,
                                  dx: ev.localDelta.dx,
                                  dy: ev.localDelta.dy,
                                  kind: ev.kind,
                                  pointer: ev.pointer
                              )
                        );
                      };
                    }
                  ),
                  LongPressGestureRecognizer: GestureRecognizerFactoryWithHandlers<LongPressGestureRecognizer>(
                          () => LongPressGestureRecognizer(postAcceptSlopTolerance: 10),
                          (instance) {
                        instance.onLongPressStart = (ev) {
                          this.onDrawingAreaPointerLongPress(
                              DrawingAreaPointerLongPressModel(
                                  x: ev.localPosition.dx,
                                  y: ev.localPosition.dy
                              )
                          );
                        };
                      }
                  )
                },
                child: Container(
                    color: Colors.white,
                    width: double.infinity,
                    height: double.infinity,
                    child: this.drawingArea
                )
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {

    final commons = <Widget>[
      Padding(
        padding: EdgeInsets.all(8),
        child: drawingSideControlPanel
      ),
      Expanded(
        child: Container(
          color: Colors.red,
          child: drawingPane(context),
        ),
      )
    ];

    return Row(
      children: isControlPanelSittingLeft ? commons : commons.reversed.toList()
    );
  }
}