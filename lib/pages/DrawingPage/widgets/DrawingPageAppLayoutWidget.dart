import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:rxdart/rxdart.dart';
import 'package:senior_project_note_app/widgets/AppLayout.dart';

class DrawingPageAppLayoutWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DrawingPageAppLayoutWidgetState();
  
  final bool isLayerUpButtonEnabled;
  final bool isLayerDownButtonEnabled;
  final bool isRotateLeftButtonEnabled;
  final bool isRotateRightButtonEnabled;
  final bool isDeleteImageButtonEnabled;
  final bool isAddImageButtonEnabled;
  final bool isUndoButtonEnable;
  final bool isRedoButtonEnable;
  final bool isOepnStaticFileButtonEnable;
  final bool isSaveStaticFileButtonEnable;

  final void Function() onRedoButtonClick;
  final void Function() onUndoButtonClick;
  final void Function() onAddImageButtonClick;
  final void Function() onDeleteImageButtonClick;
  final void Function() onRotateLeftImageButtonClick;
  final void Function() onRotateRightImageButtonClick;

  final void Function() onOpenStaticFileButtonClick;
  final void Function(String) onSaveStaticFileButtonClick;
  final void Function(String) onSavePDFFileButtonClick;

  final void Function() onImageLayerUpButtonClick;
  final void Function() onImageLayerDownButtonClick;

  final String documentName;
  final Function(String) onDocumentNameChange;

  // performance reason
  final Widget drawingPage;

  DrawingPageAppLayoutWidget({
    @required this.drawingPage,

    this.onRedoButtonClick,
    this.onUndoButtonClick,
    this.onAddImageButtonClick,
    this.onDeleteImageButtonClick,
    this.onRotateLeftImageButtonClick,
    this.onRotateRightImageButtonClick,

    this.onOpenStaticFileButtonClick,
    this.onSaveStaticFileButtonClick,
    this.onSavePDFFileButtonClick,

    this.onImageLayerUpButtonClick,
    this.onImageLayerDownButtonClick,

    this.isAddImageButtonEnabled = true,
    this.isDeleteImageButtonEnabled = true,
    this.isRotateLeftButtonEnabled = true,
    this.isRotateRightButtonEnabled = true,

    this.isLayerUpButtonEnabled = true,
    this.isLayerDownButtonEnabled = true,

    this.isRedoButtonEnable = true,
    this.isUndoButtonEnable = true,

    this.isOepnStaticFileButtonEnable = true,
    this.isSaveStaticFileButtonEnable = true,

    this.documentName,
    this.onDocumentNameChange
  });
}

class _DrawingPageAppLayoutWidgetState extends State<DrawingPageAppLayoutWidget> {
  
  final documentNameController = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    if (documentNameController.text != widget.documentName) {
      documentNameController.text = widget.documentName;
    }

    return AppLayout(
      titleWidget: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              controller: documentNameController,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold
              ),
              decoration: InputDecoration(
                hintText: 'Please Enter Document Name',
                hintStyle: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold
                )
              ),
              maxLines: 1,
            ),
          ),
          Text(" - Senior Project")
        ],
      ),

      actions: <Widget>[
        Center(
          child: Text("File")
        ),
        IconButton(
          icon: Icon(Icons.folder_open),
          onPressed: !!!widget.isOepnStaticFileButtonEnable ? null : widget.onOpenStaticFileButtonClick
        ),
        IconButton(
          icon: Icon(Icons.save_alt),
          onPressed: !!!widget.isSaveStaticFileButtonEnable ? null : () {
            widget.onSaveStaticFileButtonClick(documentNameController.text);
          }
        ),
        IconButton(
          icon: SvgPicture.asset(
            "assets/pdf.svg",
            width: 32,
            height: 32,
            color: Colors.white,
          ),
          onPressed: () {
            widget.onSavePDFFileButtonClick(documentNameController.text);
          }
        ),
        Center(
          child: Text("Image")
        ),
        IconButton(
          icon: Icon(Icons.arrow_upward),
          onPressed: !!!widget.isLayerUpButtonEnabled ? null : widget.onImageLayerUpButtonClick
        ),
        IconButton(
          icon: Icon(Icons.arrow_downward),
          onPressed: !!!widget.isLayerDownButtonEnabled ? null : widget.onImageLayerDownButtonClick
        ),
        IconButton(
          icon: Icon(Icons.rotate_left),
          onPressed: !!!widget.isRotateLeftButtonEnabled ? null : widget.onRotateLeftImageButtonClick
        ),
        IconButton(
          icon: Icon(Icons.rotate_right),
          onPressed: !!!widget.isRotateRightButtonEnabled ? null : widget.onRotateRightImageButtonClick
        ),
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: !!!widget.isDeleteImageButtonEnabled ? null : () {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text("การยืนยัน"),
                    content: Text("คุณต้องการลบรูปภาพที่เลือกใช่หรือไม่"),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("ลบ", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)),
                        onPressed: () { widget.onDeleteImageButtonClick(); Navigator.of(context).pop(); },
                      ),
                      FlatButton(
                        child: Text("ยกเลิก"),
                        onPressed: () { Navigator.of(context).pop(); },
                      )
                    ],
                  );
                }
            );
          },
        ),
        IconButton(
          icon: Icon(Icons.image),
          onPressed: !!!widget.isAddImageButtonEnabled ? null : widget.onAddImageButtonClick
        ),
        Center(
            child: Text("Edit")
        ),
        IconButton(
          icon: Icon(Icons.undo),
          onPressed: !!!widget.isUndoButtonEnable ? null : widget.onUndoButtonClick
        ),
        IconButton(
          icon: Icon(Icons.redo),
          onPressed: !!!widget.isRedoButtonEnable ? null : widget.onRedoButtonClick
        ),
      ],
      child: widget.drawingPage
    );
  }
}