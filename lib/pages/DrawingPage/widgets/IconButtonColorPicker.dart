import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class IconButtonColorPicker extends StatefulWidget {

  final Color selectedColor;
  final Color initialColor;
  final void Function(Color color) onPressed;

  IconButtonColorPicker({
    this.selectedColor,
    this.initialColor,
    this.onPressed
  });

  @override
  State<IconButtonColorPicker> createState() => _IconButtonColorPickerState();
}

class _IconButtonColorPickerState extends State<IconButtonColorPicker> {
  Color _currentColor;

  @override
  void initState() {
    super.initState();
    _currentColor = widget.initialColor;
  }

  Future<Color> _showColorPickerDialog(BuildContext context, [ Color color = Colors.black ]) async {
    Color pickedColor = color;
    await showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Pick a color!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: pickedColor,
            onColorChanged: (color) { pickedColor = color; },
            enableLabel: true,
            pickerAreaHeightPercent: 0.8,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: const Text('Got it'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
    return pickedColor;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () async {
        final pickedColor = await _showColorPickerDialog(context);
        print(pickedColor);
        setState(() {
          _currentColor = pickedColor;
        });
      },
      child: IconButton(
        icon: Icon(widget.selectedColor == _currentColor ? Icons.check_circle : Icons.check_circle_outline),
        color: _currentColor,
        onPressed: () {
          widget.onPressed(_currentColor);
        }
      )
    );
  }
}