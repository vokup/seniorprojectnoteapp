import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingTool.dart';

class DrawingControlPanel extends StatelessWidget {

  final DrawingTools curDrawingTool;
  final void Function(DrawingTools) onDrawingToolChange;

  final double drawingThickness;
  final Color drawingColor;
  final void Function(Color) onDrawingColorChange;
  final void Function(double) onDrawingThicknessChange;

  final void Function(double, double) onControlPanelMove;

  DrawingControlPanel({
    this.curDrawingTool,
    this.onDrawingToolChange,
    this.drawingThickness,
    this.drawingColor,

    this.onDrawingThicknessChange,
    this.onDrawingColorChange,

    this.onControlPanelMove
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onPanUpdate: (ev) {
        this.onControlPanelMove(ev.delta.dx, ev.delta.dy);
      },
      child: Container(
        decoration: BoxDecoration(
          color: Color.fromARGB(200, 220, 220, 220),
          borderRadius: BorderRadius.all(Radius.circular(8))
        ),
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("Tools: "),
                  IconButton(
                      icon: SvgPicture.asset(
                          "assets/text.svg",
                          color: this.curDrawingTool == DrawingTools.ConvertingPen ? Colors.black : Colors.grey,
                          width: 32,
                          height: 32
                      ),
                      onPressed: () {
                        this.onDrawingToolChange(DrawingTools.ConvertingPen);
                      }
                  ),
                  IconButton(
                    icon: SvgPicture.asset(
                      "assets/normal-pen.svg",
                      color: this.curDrawingTool == DrawingTools.NormalPen ? Colors.black : Colors.grey,
                      width: 32,
                      height: 32
                    ),
                    onPressed: () {
                      this.onDrawingToolChange(DrawingTools.NormalPen);
                    }
                  ),
                  IconButton(
                    icon: SvgPicture.asset(
                      "assets/eraser.svg",
                      color: this.curDrawingTool == DrawingTools.Eraser ? Colors.black : Colors.grey,
                      width: 32,
                      height: 32
                    ),
                    onPressed: () {
                      this.onDrawingToolChange(DrawingTools.Eraser);
                    }
                  ),
                  IconButton(
                    icon: SvgPicture.asset(
                        "assets/select.svg",
                        color: this.curDrawingTool == DrawingTools.Selecting ? Colors.black : Colors.grey,
                        width: 32,
                        height: 32
                    ),
                    onPressed: () {
                      this.onDrawingToolChange(DrawingTools.Selecting);
                    }
                  )
                ]
              ),
              Row(
                children: <Widget>[
                  Text("Current Thickness: ${this.drawingThickness}"),
                ]
              ),
              Row(
                children: <Widget>[
                  Text("Thickness: 1"),
                  Slider(
                    onChanged: (ev) { this.onDrawingThicknessChange(ev.round().toDouble()); },
                    divisions: 9,
                    min: 1,
                    max: 10,
                    value: drawingThickness
                  ),
                  Text("10"),
                ]
              ),
              Row(
                children: <Widget>[
                  Text("Color: "),
                  IconButton(
                    icon: Icon(drawingColor == Colors.black ? Icons.check_circle : Icons.check_circle_outline),
                    color: Colors.black,
                    onPressed: () {
                      this.onDrawingColorChange(Colors.black);
                    }
                  ),
                  IconButton(
                    icon: Icon(drawingColor == Colors.red ? Icons.check_circle : Icons.check_circle_outline),
                    color: Colors.red,
                    onPressed: () {
                      this.onDrawingColorChange(Colors.red);
                    }
                  ),
                  IconButton(
                    icon: Icon(drawingColor == Colors.green ? Icons.check_circle : Icons.check_circle_outline),
                    color: Colors.green,
                    onPressed: () {
                      this.onDrawingColorChange(Colors.green);
                    }
                  ),
                  IconButton(
                    icon: Icon(drawingColor == Colors.blue ? Icons.check_circle : Icons.check_circle_outline),
                    color: Colors.blue,
                    onPressed: () {
                      this.onDrawingColorChange(Colors.blue);
                    }
                  )
                ]
              )
            ]
          )
        )
      )
    );
  }
}