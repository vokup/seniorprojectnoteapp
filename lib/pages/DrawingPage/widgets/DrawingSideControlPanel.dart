import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_svg/flutter_svg.dart' as svg;
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingTool.dart';
import 'package:senior_project_note_app/pages/DrawingPage/widgets/IconButtonColorPicker.dart';

class DrawingSideControlPanel extends StatelessWidget {
  final bool isSittingLeft;
  final void Function() onChangeSittingSideClick;

  final DrawingTools curDrawingTool;
  final void Function(DrawingTools) onDrawingToolChange;

  final double drawingThickness;
  final Color drawingColor;
  final void Function(Color) onDrawingColorChange;
  final void Function(double) onDrawingThicknessChange;

  final void Function(double, double) onControlPanelMove;

  DrawingSideControlPanel({
    this.isSittingLeft = true,
    this.onChangeSittingSideClick,

    this.curDrawingTool,
    this.onDrawingToolChange,
    this.drawingThickness = 1,
    this.drawingColor,

    this.onDrawingThicknessChange,
    this.onDrawingColorChange,

    this.onControlPanelMove
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          IconButton(
            icon: isSittingLeft ? Icon(Icons.arrow_forward) : Icon(Icons.arrow_back),
            onPressed: onChangeSittingSideClick
          ),
          Text("Tools", style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            children: <Widget>[
              IconButton(
                  icon: svg.SvgPicture.asset(
                      "assets/text-1.svg",
                      color: this.curDrawingTool == DrawingTools.ConvertingPen ? Colors.black : Colors.grey,
                      width: 32,
                      height: 32
                  ),
                  onPressed: () {
                    this.onDrawingToolChange(DrawingTools.ConvertingPen);
                  }
              ),
              IconButton(
                  icon: svg.SvgPicture.asset(
                      "assets/pen.svg",
                      color: this.curDrawingTool == DrawingTools.NormalPen ? Colors.black : Colors.grey,
                      width: 32,
                      height: 32
                  ),
                  onPressed: () {
                    this.onDrawingToolChange(DrawingTools.NormalPen);
                  }
              ),
            ],
          ),
          Row(
            children: <Widget>[
              IconButton(
                  icon: svg.SvgPicture.asset(
                      "assets/eraser.svg",
                      color: this.curDrawingTool == DrawingTools.Eraser ? Colors.black : Colors.grey,
                      width: 32,
                      height: 32
                  ),
                  onPressed: () {
                    this.onDrawingToolChange(DrawingTools.Eraser);
                  }
              ),
              IconButton(
                  icon: svg.SvgPicture.asset(
                      "assets/select.svg",
                      color: this.curDrawingTool == DrawingTools.Selecting ? Colors.black : Colors.grey,
                      width: 32,
                      height: 32
                  ),
                  onPressed: () {
                    this.onDrawingToolChange(DrawingTools.Selecting);
                  }
              )
            ],
          ),
          SizedBox(height: 12),
          Text("Thickness", style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            children: <Widget>[
              RotatedBox(
                quarterTurns: 1,
                child: Slider(
                    onChanged: (ev) { this.onDrawingThicknessChange(ev.round().toDouble()); },
                    divisions: 9,
                    min: 1,
                    max: 10,
                    value: drawingThickness
                ),
              ),
              Text(
                drawingThickness.toString(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20
                ),
              )
            ],
          ),
          Text("Colors", style: TextStyle(fontWeight: FontWeight.bold)),
          Row(
            children: <Widget>[
              IconButton(
                  icon: Icon(drawingColor == Colors.black ? Icons.check_circle : Icons.check_circle_outline),
                  color: Colors.black,
                  onPressed: () {
                    this.onDrawingColorChange(Colors.black);
                  }
              ),
              IconButton(
                  icon: Icon(drawingColor == Colors.red ? Icons.check_circle : Icons.check_circle_outline),
                  color: Colors.red,
                  onPressed: () {
                    this.onDrawingColorChange(Colors.red);
                  }
              ),
            ],
          ),
          Row(
            children: <Widget>[
              IconButton(
                  icon: Icon(drawingColor == Colors.green ? Icons.check_circle : Icons.check_circle_outline),
                  color: Colors.green,
                  onPressed: () {
                    this.onDrawingColorChange(Colors.green);
                  }
              ),
              IconButton(
                  icon: Icon(drawingColor == Colors.blue ? Icons.check_circle : Icons.check_circle_outline),
                  color: Colors.blue,
                  onPressed: () {
                    this.onDrawingColorChange(Colors.blue);
                  }
              )
            ],
          ),
          Row(
            children: <Widget>[
              IconButtonColorPicker(
                initialColor: Colors.orange,
                selectedColor: drawingColor,
                onPressed: (color) {
                  print(color);
                  this.onDrawingColorChange(color);
                }
              ),
              IconButtonColorPicker(
                initialColor: Colors.purple,
                selectedColor: drawingColor,
                onPressed: (color) {
                  this.onDrawingColorChange(color);
                }
              ),
            ],
          ),
        ],
      )
    );
  }
}