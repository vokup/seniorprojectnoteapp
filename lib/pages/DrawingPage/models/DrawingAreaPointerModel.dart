import 'dart:ui';

import 'package:meta/meta.dart';

class DrawingAreaPointerModel {
  final int pointer;
  final double x;
  final double y;
  final double dx;
  final double dy;
  final PointerDeviceKind kind;

  DrawingAreaPointerModel({ 
    @required this.x, 
    @required this.y, 
    @required this.dx,
    @required this.dy,
    @required this.kind ,
    this.pointer = -1
  });
}

class DrawingAreaPointerUpModel extends DrawingAreaPointerModel {
  final int pointerCount;
  
  DrawingAreaPointerUpModel({ 
    @required this.pointerCount,
    int pointer,
    double x,
    double y,
    double dx,
    double dy,
    PointerDeviceKind kind
  }): super(x: x, y: y, dx: dx, dy: dy, kind: kind);
}

class DrawingAreaPointerLongPressModel {
  final double x;
  final double y;

  DrawingAreaPointerLongPressModel({
    @required this.x,
    @required this.y
  });
}