enum DrawingState {
  Ready,
  Drawing,
  Erasing,
  SelectingImage,
  ResizingImage
}