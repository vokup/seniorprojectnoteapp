import 'package:meta/meta.dart';

@immutable
class DrawingControlPanelState {
  final double x;
  final double y;

  final bool isSittingLeft;

  DrawingControlPanelState({ 
    @required this.x, 
    @required this.y,
    @required this.isSittingLeft  
  });

  factory DrawingControlPanelState.initial() {
    return DrawingControlPanelState(x: 0, y: 0, isSittingLeft: true);
  }

  DrawingControlPanelState relativeMove(double x, double y) {
    return copyWith(x: x, y: y);
  }

  DrawingControlPanelState copyWith({
    double x,
    double y,
    bool isSittingLeft
  }) {
    return DrawingControlPanelState(
      x: x ?? this.x, 
      y: y ?? this.y,
      isSittingLeft: isSittingLeft ?? this.isSittingLeft
    );
  }
}