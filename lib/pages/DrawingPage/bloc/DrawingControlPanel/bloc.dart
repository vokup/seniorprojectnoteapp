import 'package:bloc/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingControlPanel/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingControlPanel/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/bloc.dart';

class DrawingControlPanelBloc extends Bloc<DrawingControlPanelEvent, DrawingControlPanelState> {
  @override
  DrawingControlPanelState get initialState => DrawingControlPanelState.initial();

  final DrawingPropertiesBloc drawingPropertiesBloc;

  DrawingControlPanelBloc(this.drawingPropertiesBloc);

  @override
  Stream<DrawingControlPanelState> mapEventToState(DrawingControlPanelEvent event) async* {
    if (event is MoveControlPanelByDelta) {
      yield state.copyWith(x: state.x + event.x, y: state.y + event.y);
    } else if (event is ChangeControlPanelSide) {
      yield state.copyWith(isSittingLeft: event.isSittingLeft);
    }
  }
}