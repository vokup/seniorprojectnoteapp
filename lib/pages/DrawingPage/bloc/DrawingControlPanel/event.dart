import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

abstract class DrawingControlPanelEvent extends Equatable {
  @override
  List<Object> get props => _props;
  final List _props;

  DrawingControlPanelEvent([List props = const []]):
        _props = props;
}

class MoveControlPanelByDelta extends DrawingControlPanelEvent {
  final double x;
  final double y;
  MoveControlPanelByDelta({ @required this.x, @required this.y }) : super([ Point<double>(x, y) ]);
}

class ChangeControlPanelSide extends DrawingControlPanelEvent {
  final bool isSittingLeft;
  ChangeControlPanelSide({
    @required this.isSittingLeft
  });
}