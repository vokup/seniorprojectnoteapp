import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:image/image.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:senior_project_note_app/data/ConvertedTextRepository.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/drawables/ImageDrawable.dart';
import 'package:senior_project_note_app/drawables/PenDrawable.dart';
import 'package:senior_project_note_app/drawables/TextDrawable.dart';
import 'package:senior_project_note_app/libs/LinkedListExt.dart';
import 'package:senior_project_note_app/libs/PDFWriter.dart';
import 'package:senior_project_note_app/libs/StaticFileReader.dart';
import 'package:senior_project_note_app/libs/StaticFileWriter.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/StaticFile/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/StaticFile/state.dart';
import 'package:senior_project_note_app/pages/Global/bloc/bloc.dart';
import 'package:senior_project_note_app/pages/Global/bloc/event.dart';
import 'package:senior_project_note_app/services/DrawableIDProviderService.dart';
import 'package:senior_project_note_app/services/SaveFileDialogService.dart';
import 'package:senior_project_note_app/utils/IterableConverter.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;

class StaticFileBloc extends Bloc<StaticFileEvent, StaticFileState> {
  @override
  StaticFileState get initialState => StaticFileState.initial();

  final ConvertedTextRepository _convertedTextRepository;

  final DrawingAreaBloc _drawingAreaBloc;
  final GlobalBloc _globalBloc;
  final DrawingToolbarBloc _drawingToolbarBloc;

  final DrawableIDProviderService _drawableIDProviderService;

  StaticFileBloc(
    DrawingAreaBloc dab, 
    GlobalBloc gb, 
    DrawingToolbarBloc dtb,
    ConvertedTextRepository convertedTextRepository,
    DrawableIDProviderService drawableIDProviderService
  ):
    _drawingAreaBloc = dab,
    _globalBloc = gb,
    _drawingToolbarBloc = dtb,
    _convertedTextRepository = convertedTextRepository,
    _drawableIDProviderService = drawableIDProviderService;

  @override
  Stream<StaticFileState> mapEventToState(StaticFileEvent event) async* {
    if (event is SaveStaticFile) {
      yield* _handleSaveStaticFile(event);
    } else if (event is OpenStaticFile) {
      yield* _handleOpenStaticFile(event);
    } else if (event is SavePDFFile) {
      yield* _handleSavePDFFile(event);
    }
  }

  Stream<StaticFileState> _handleOpenStaticFile(OpenStaticFile event) async* {
    _globalBloc.add(ShowProgressDialog("Opening File ..."));
    final path = event.absolutePath;
    final dangerousFileName = path.split("/").last.replaceAll(".ku", "");
    final reader = StaticFileReader();
    final savedData = await reader.read(path);

    _drawingAreaBloc.add(
      SetDrawableListFromOpenStaticFile(
        latestDrawableId: savedData.latestDrawableId,
        penDrawables: savedData.penDrawables,
        textDrawables: savedData.textDrawables,
        imageDrawables: savedData.imageDrawables        
      )
    );

    _drawingToolbarBloc.add(UpdateDocumentName(dangerousFileName));

    _globalBloc.add(HideProgressDialog());
  }

  Stream<StaticFileState> _handleSaveStaticFile(SaveStaticFile event) async* {
    final permissions = await PermissionHandler().requestPermissions([PermissionGroup.storage]);
    if (permissions[PermissionGroup.storage] != PermissionStatus.granted) {
      return;
    }

    _globalBloc.add(ShowProgressDialog("Saving File ..."));
    // final dir = await pathProvider.getExternalStorageDirectory();
    // final absPath = "${dir.path}/test.ku";
    final dab = _drawingAreaBloc;
    final drawables = IterableConverter<DrawableEntry, Drawable>(dab.state.drawables, (x) => x.drawable);
    final serializer = StaticFileWriter(drawables, _drawableIDProviderService.currentId);
    // await serializer.save(absPath);
    // print(absPath);

    final savedBytes = await serializer.saveAsBytes();
    
    String savedFileName;
    try {
      savedFileName = await SaveFileDialogService().save(
        name: "${event.documentName}.ku", 
        mime: "application/ku",
        data: Uint8List.fromList(savedBytes)
      );
    } catch (e) {
      savedFileName = null;
    }

    if (savedFileName != null) {
      print("SavedFileName: $savedFileName");
      _drawingToolbarBloc.add(UpdateDocumentName(savedFileName.replaceAll(".ku", "")));
    }

    _globalBloc.add(HideProgressDialog());
  }

  Stream<StaticFileState> _handleSavePDFFile(SavePDFFile event) async* {
    final permissions = await PermissionHandler().requestPermissions([PermissionGroup.storage]);
    if (permissions[PermissionGroup.storage] != PermissionStatus.granted) {
      return;
    }

    _globalBloc.add(ShowProgressDialog("Exporting PDF ..."));

    final writer = PDFWriter();
    await writer.intialize();

    final drawables = IterableConverter<DrawableEntry, Drawable>(
      _drawingAreaBloc.state.drawables, 
      (x) => x.drawable
    );

    for (final d in drawables) {
      if (d is PenDrawable) {
        writer.addPath(d.points.map((i) => Offset(i.x, i.y)), d.paint.strokeWidth, d.paint.color);
      } else if (d is TextDrawable) {
        // final model = _convertedTextRepository.lookup(d.groupId);
        writer.addString(d.text, d.getBound().topLeft, d.getBound().height, d.color);
      } else if (d is ImageDrawable) {
        final imageBytes = await d.image.toByteData(format: ImageByteFormat.png);
        writer.addImage(
          imageBytes.buffer.asUint8List(),
          d.offset, 
          Offset(d.size.width, d.size.height)
        );
      }
    }

    // final dir = await pathProvider.getExternalStorageDirectories(type: pathProvider.StorageDirectory.documents);
    // final dirPath = dir.first.path;
    // final pdfPath = "${dirPath}/mypdf.pdf";
    
    // writer.save(pdfPath);
    // await Future.delayed(Duration(seconds: 1));
    // print("SAVED: $pdfPath");

    try {
      await SaveFileDialogService().save(
        name: "${event.documentName}.pdf", 
        mime: "application/pdf",
        data: Uint8List.fromList(writer.saveAsBytes())
      );
    } catch (e) {
      print("[PDFWriter/Error]: $e");
    }

    _globalBloc.add(HideProgressDialog());
  }
}