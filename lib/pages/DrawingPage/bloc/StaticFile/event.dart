import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class StaticFileEvent extends Equatable {
  @override
  List<Object> get props => _props;
  final List _props;

  StaticFileEvent([List props = const []]):
        _props = props;
}

class OpenStaticFile extends StaticFileEvent {
  final String absolutePath;
  OpenStaticFile(this.absolutePath): super([ absolutePath ]);
}

class SaveStaticFile extends StaticFileEvent {
  final String documentName;
  SaveStaticFile(this.documentName): super([ documentName ]);
}

class SavePDFFile extends StaticFileEvent {
  final String documentName;
  SavePDFFile(this.documentName): super([ documentName ]);
}