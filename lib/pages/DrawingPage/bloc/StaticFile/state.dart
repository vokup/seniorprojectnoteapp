import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
class StaticFileState {
  StaticFileState();

  factory StaticFileState.initial() {
    return StaticFileState();
  }
}