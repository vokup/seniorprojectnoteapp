import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:bloc/bloc.dart';
import 'package:senior_project_note_app/data/ConvertedTextRepository.dart';
import 'package:senior_project_note_app/data/DrawableGroupRepository.dart';

import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/drawables/EmptyDrawable.dart';
import 'package:senior_project_note_app/drawables/ImageDrawable.dart';
import 'package:senior_project_note_app/drawables/PenDrawable.dart';
import 'package:senior_project_note_app/drawables/TextDrawable.dart';
import 'package:senior_project_note_app/libs/DrawableGrabber.dart';
import 'package:senior_project_note_app/libs/DrawingAreaScaler.dart';
import 'package:senior_project_note_app/libs/ImageResizingResolver.dart';
import 'package:senior_project_note_app/libs/LinkedListExt.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingState.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingTool.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/ImageSourceType.dart';
import 'package:senior_project_note_app/pages/Global/bloc/bloc.dart';
import 'package:senior_project_note_app/pages/Global/bloc/event.dart';
import 'package:senior_project_note_app/services/DrawableIDProviderService.dart';
import 'package:senior_project_note_app/services/GroupQueueService.dart';
import 'package:senior_project_note_app/services/GroupQueueWorker.dart';
import 'package:senior_project_note_app/services/TensorflowService.dart';
import 'package:senior_project_note_app/services/WordGroupingService.dart';
import 'package:senior_project_note_app/utils/GeometryCalculation.dart';

import 'package:senior_project_note_app/utils/ImageRotator.dart';
import 'package:senior_project_note_app/utils/IterableConverter.dart';

class DrawingAreaBloc extends Bloc<DrawingAreaEvent, DrawingAreaState> {
  @override
  DrawingAreaState get initialState => DrawingAreaState.initial();

  final DrawingPropertiesBloc _drawingPropertiesBloc;
  final GlobalBloc _globalBloc;

  final DrawableIDProviderService _drawableIDProviderService;
  final WordGroupingService _wordGroupingService;
  final GroupQueueService _groupQueueService;
  
  final DrawableGroupRepository _drawableGroupRepository;
  final ConvertedTextRepository _convertedTextRepository;

  final AreaScaler _areaScaler;
  
  GroupQueueWorker _groupQueueWorker;

  DrawingAreaBloc(
    this._drawingPropertiesBloc,
    this._globalBloc,
    this._drawableIDProviderService,
    this._wordGroupingService,
    this._drawableGroupRepository,
    this._convertedTextRepository,
    this._groupQueueService,
    this._areaScaler
  ) {
    _groupQueueWorker = GroupQueueWorker(
      this, 
      _drawableGroupRepository,
      _convertedTextRepository,
      _areaScaler
    );
  }

  @override
  Stream<DrawingAreaState> mapEventToState(DrawingAreaEvent event) async* {
    if (event is PointerDown) { yield* _handlePointerDown(event); }
    else if (event is PointerUp) { yield* _handlePointerUp(event); }
    else if (event is PointerMove) { yield* _handlePointerMove(event); }
    else if (event is UndoDrawing) { yield* _handleUndoDrawing(event); }
    else if (event is RedoDrawing) { yield* _handleRedoDrawing(event); }
    else if (event is LongPress) { yield* _handleLongPress(event); }
    else if (event is AddImage) { yield* _handleAddImage(event); }
    else if (event is DeleteImage) { yield* _handleDeleteImage(event); }
    else if (event is RotateLeftImage) { yield* _handleRotateLeftImage(event); }
    else if (event is RotateRightImage) { yield* _handleRotateRightImage(event); }
    else if (event is SetDrawableListFromOpenStaticFile) { yield* _handleSetDrawableListFromOpenStaticFile(event); }
    else if (event is OnPageCreated) { yield* _handleOnPageCreated(event); }
    else if (event is OnPageResumed) { yield* _handleOnPageResumed(event); }
    else if (event is OnPagePaused) { yield* _handleOnPagePaused(event); }
    else if (event is AddConvertedText) { yield* _handleAddConvertedText(event); }
    else if (event is SetDrawingAreaSize) { yield* _handleSetDrawingAreaSize(event); }
    else if (event is MoveLayerUp) { yield* _handleMoveLayerUp(event); }
    else if (event is MoveLayerDown) { yield* _handleMoveLayerDown(event); }
  }

  Stream<DrawingAreaState> _handleMoveLayerUp(MoveLayerUp event) async* {
    if (!!!(state.curSelectingImageDrawableEntry.drawable is EmptyDrawable)) {
      print("Layer Up: ${state.curSelectingImageDrawableEntry.drawable.id}");

      final imageList = state.imageDrawableList.set;
      ImageDrawable curSelectingImage = state.curSelectingImageDrawableEntry.drawable;
      for (final img in IterableConverter<DrawableEntry, ImageDrawable>(imageList, (x) => x.drawable)) {
        final did = img.id;
        if (did > curSelectingImage.id) {
          final newSelecting = curSelectingImage.copyWith(drawableId: did);
          final otherImage = img.copyWith(drawableId: curSelectingImage.id);

          yield state.copyWith(
            imageDrawableList: state.imageDrawableList.copyWith((list) {
              list.remove(DrawableEntry(img));
              list.remove(DrawableEntry(curSelectingImage));
              list.add(DrawableEntry(newSelecting));
              list.add(DrawableEntry(otherImage));
            }),
            curSelectingImageDrawableEntry: DrawableEntry(newSelecting)
          );
          break;
        }
      }
    }
  }

  Stream<DrawingAreaState> _handleMoveLayerDown(MoveLayerDown event) async* {
    if (!!!(state.curSelectingImageDrawableEntry.drawable is EmptyDrawable)) {
      print("Layer Down: ${state.curSelectingImageDrawableEntry.drawable.id}");

      final imageList = state.imageDrawableList.set;
      ImageDrawable curSelectingImage = state.curSelectingImageDrawableEntry.drawable;
      ImageDrawable otherImage;
      for (final img in IterableConverter<DrawableEntry, ImageDrawable>(imageList, (x) => x.drawable)) {
        final did = img.id;
        if (did < curSelectingImage.id) {
          otherImage = img;
        } else {
          break;
        }
      }

      if (otherImage != null) {
        final newSelecting = curSelectingImage.copyWith(drawableId: otherImage.id);
        final newOtherImage = otherImage.copyWith(drawableId: curSelectingImage.id);

        yield state.copyWith(
          imageDrawableList: state.imageDrawableList.copyWith((list) {
            list.remove(DrawableEntry(curSelectingImage));
            list.remove(DrawableEntry(otherImage));
            list.add(DrawableEntry(newSelecting));
            list.add(DrawableEntry(newOtherImage));
          }),
          curSelectingImageDrawableEntry: DrawableEntry(newSelecting)
        );
      }
    }
  }

  Stream<DrawingAreaState> _handleSetDrawingAreaSize(SetDrawingAreaSize event) async* {
    if (event.width != state.drawingAreaSize.width || event.height != state.drawingAreaSize.height) {

      _areaScaler.setWorkingAreaSize(event.width, event.height);

      yield state.copyWith(
        drawingAreaSize: Size(event.width, event.height)
      );
    }
  }

  Stream<DrawingAreaState> _handleAddConvertedText(AddConvertedText event) async* {
    yield state.copyWith(
      textDrawableList: state.textDrawableList.copyWith((list) {
        final id = _drawableIDProviderService.getID();
        list.add(DrawableEntry(
          TextDrawable(
            id, 
            text: event.text,
            fontSize: event.fontSize,
            offset: event.offset,
            renderedBound: event.renderedBound,
            color: event.color,
            groupId: event.groupId
          )
        ));
      }),
      penDrawableList: state.penDrawableList.copyWith((list) {
        for (final rd in event.relatedDrawables) {
          list.remove(DrawableEntry(rd));
        }
      })
    );
  }

  Stream<DrawingAreaState> _handleOnPageCreated(OnPageCreated event) async* {
    // _globalBloc.add(ShowProgressDialog("Starting ..."));

    print("DrawingAreaBloc: OnPageStart");

    print("TFLiteSerivce: Model Loadeding");
    final loadModelResult = await TFLiteService.loadModel(
      "assets/ml/florres_htr_final_apostrophe.tflite",
      // "assets/ml/florres_htr_final.tflite",
      "assets/ml/ctc_decoded_model_beam_width_1_florres.tflite"
    );
    print("TFLiteSerivce: Model Loaded: ${loadModelResult}");

    _wordGroupingService.groupStream.listen((gsm) {
      if (gsm.isAdding) {
        print("DrawingAreaBloc: addGroupStream [${gsm.groupModel.id}]");
        _groupQueueService.add(gsm.groupModel.id);
      } else {
        print("DrawingAreaBloc: removeGroupStream [${gsm.groupModel.id}]");
        _groupQueueService.remove(gsm.groupModel.id);
      }
    });

    _groupQueueService.start(_groupQueueWorker);

    // _globalBloc.add(HideProgressDialog());
  }

  Stream<DrawingAreaState> _handleOnPageResumed(OnPageResumed event) async* {
    _groupQueueService.start(_groupQueueWorker);
  }

  Stream<DrawingAreaState> _handleOnPagePaused(OnPagePaused event) async* {
    await _groupQueueService.stop();
  }

  Stream<DrawingAreaState> _handlePointerDown(PointerDown event) async* {
    final dp = _drawingPropertiesBloc.state;
    if (dp.curDrawingState == DrawingState.Ready) {
      switch (dp.curDrawingTool) {
        case DrawingTools.ConvertingPen:
        case DrawingTools.NormalPen: {
          if (event.kind == PointerDeviceKind.stylus) {
            _drawingPropertiesBloc.add(new SetDrawingState(state: DrawingState.Drawing));
            yield state.copyWith(
              curWorkingPointer: event.pointer,
              curWorkingDrawable: PenDrawable(
                drawableId: _drawableIDProviderService.getID(),
                isNormalPen: dp.curDrawingTool == DrawingTools.NormalPen,
                sx: _areaScaler.getTargetX(event.x),
                sy: _areaScaler.getTargetY(event.y),
                color: dp.curDrawingColor,
                strokeWidth: dp.curDrawingThickness + 1
              ),
              undoDrawables: LinkedListExt<DrawableEntry>.empty()
            );
          }
        } break;
        case DrawingTools.Eraser: {
          _drawingPropertiesBloc.add(new SetDrawingState(state: DrawingState.Erasing));
          yield* _handleEraser(_areaScaler.getTargetX(event.x), _areaScaler.getTargetY(event.y), event.pointer);
        } break;
        default:
          break;
      }
    } 
    else if (dp.curDrawingState == DrawingState.SelectingImage) {
      DrawableEntry imgEntry = state.curSelectingImageDrawableEntry;
      ImageDrawable imgDrawable = imgEntry.drawable;
      if (dp.curDrawingTool != DrawingTools.Selecting) {
        _drawingPropertiesBloc.add(SetDrawingState(state: DrawingState.Ready));
        yield* _imageDeselectHelper(imgEntry);
      } else {
        final resizingPosition = ImageDrawableResizingResolver
                                  .resolvePosition(
                                    imgDrawable, Point(
                                      _areaScaler.getTargetX(event.x),
                                      _areaScaler.getTargetY(event.y)
                                    )
                                  );

        if (resizingPosition != ResizingPosition.None) {
          yield state.copyWith(
            curWorkingPointer: event.pointer,
            curResizingPosition: resizingPosition
          );
        } else if (
          !!!imgDrawable
          .getBound()
          .contains(
            Offset(
              _areaScaler.getTargetX(event.x), 
              _areaScaler.getTargetY(event.y)
            )
          )
        ) {
          // deselect
          _drawingPropertiesBloc.add(SetDrawingState(state: DrawingState.Ready));
          yield * _imageDeselectHelper(imgEntry);
        } else {
          // this is not resizing operation, but it is moving
          yield state.copyWith(
            curWorkingPointer: event.pointer,
          );
        }
      }
    }
  }

  Stream<DrawingAreaState> _handlePointerUp(PointerUp event) async* {
    final dp = _drawingPropertiesBloc.state;
    print(dp.curDrawingState);
    if (dp.curDrawingState == DrawingState.Drawing || dp.curDrawingState == DrawingState.Erasing) {
      // when all fingers are lift-up
      if (event.pointerCount == 0) {
        if (dp.curDrawingTool == DrawingTools.NormalPen
            || dp.curDrawingTool == DrawingTools.ConvertingPen
        ) {
          yield* _addToDrawableListHelper(state.curWorkingDrawable);
          yield state.copyWith(
            curWorkingDrawable: EmptyDrawable()
          );
        }
        _drawingPropertiesBloc.add(SetDrawingState(state: DrawingState.Ready));
      }
    }
    if (dp.curDrawingState == DrawingState.SelectingImage) {
      if (state.curResizingPosition != ResizingPosition.None) {
        yield state.copyWith(
          curResizingPosition: ResizingPosition.None
        );
      }
    }
  }

  Stream<DrawingAreaState> _handlePointerMove(PointerMove event) async*  {
    final dp = _drawingPropertiesBloc.state;
    if (dp.curDrawingState == DrawingState.Drawing || dp.curDrawingState == DrawingState.Erasing) {
      if (event.pointer != state.curWorkingPointer) { return; }
      switch (dp.curDrawingTool) {
        case DrawingTools.ConvertingPen:
        case DrawingTools.NormalPen: {
          final curWorkingDrawable = state.curWorkingDrawable;
          if (curWorkingDrawable is PenDrawable) {
              yield state.copyWith(
              curWorkingDrawable: curWorkingDrawable.copyWith()
                                    ..addRelativePoint(
                                      Point(
                                        _areaScaler.getTargetX(event.dx), 
                                        _areaScaler.getTargetY(event.dy)
                                      )
                                    )
            );
          }
        } break;
        case DrawingTools.Eraser: {
            // don't pass pointer (you must pass pointer only in POINTER_DOWN event!)
            yield* _handleEraser(
              _areaScaler.getTargetX(event.x), 
              _areaScaler.getTargetY(event.y), 
              null
            );
        } break;
        default:
          break;
      }
    } else if (dp.curDrawingState == DrawingState.SelectingImage
      && dp.curDrawingTool == DrawingTools.Selecting
      && state.curWorkingPointer == event.pointer
    ) {
      yield* _handlePointerMoveOnImageResizing(event);
    }
  }

  Stream<DrawingAreaState> _handlePointerMoveOnImageResizing(PointerMove event) async* {
    if (state.curResizingPosition == ResizingPosition.None) {
      // move image
      DrawableEntry imgEntry = state.curSelectingImageDrawableEntry;
      ImageDrawable imgDrawable = imgEntry.drawable;
      Offset newOffset = imgDrawable.offset + Offset(_areaScaler.getTargetX(event.dx), _areaScaler.getTargetY(event.dy));
      yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
          offset: newOffset
      ));
    } else {
      // resize image
      DrawableEntry imgEntry = state.curSelectingImageDrawableEntry;
      ImageDrawable imgDrawable = imgEntry.drawable;

      final ex = _areaScaler.getTargetX(event.x);
      final ey = _areaScaler.getTargetY(event.y);
      switch (state.curResizingPosition) {
        case ResizingPosition.TopLeft: {
          final aspectRatio = imgDrawable.getBound().width / imgDrawable.getBound().height;
          final topLeftOffset = imgDrawable.getBound().topLeft;
          final dx = ex - topLeftOffset.dx;
          final dwidth = -dx;
          final dheight = dwidth / aspectRatio;
          final nextWidth = imgDrawable.size.width + dwidth;
          final nextHeight = imgDrawable.size.height + dheight;
          if (nextWidth > 0 && nextHeight > 0) {
            final nextOffset = imgDrawable.getBound().topLeft + Offset(-dwidth, -dheight);
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(nextWidth, nextHeight),
                offset: nextOffset
            ));
          }
        } break;
        case ResizingPosition.Top: {
          final oriTopLeft = imgDrawable.getBound().topLeft;
          final oriSize = Offset(imgDrawable.getBound().width, imgDrawable.getBound().height);
          final distance = ey - oriTopLeft.dy;
          final distanceOffset = Offset(0, distance);
          final nextSize = oriSize - distanceOffset;
          if (nextSize.dy > 0) {
            final nextOffset = imgDrawable.getBound().topLeft + distanceOffset;
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(nextSize.dx, nextSize.dy),
                offset: nextOffset
            ));
          }
        } break;
        case ResizingPosition.TopRight: {
          final aspectRatio = imgDrawable.getBound().width / imgDrawable.getBound().height;
          final oriTopRight = imgDrawable.getBound().topRight;

          final dx = ex - oriTopRight.dx;
          final dwidth = dx;
          final dheight = dwidth / aspectRatio;
          final nextWidth = imgDrawable.size.width + dwidth;
          final nextHeight = imgDrawable.size.height + dheight;
          if (nextWidth > 0 && nextHeight > 0) {
            final nextOffset = imgDrawable.getBound().topLeft + Offset(0, -dheight);
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(nextWidth, nextHeight),
                offset: nextOffset
            ));
          }
        } break;
        case ResizingPosition.Left: {
          final topLeftOffset = imgDrawable.getBound().topLeft;
          final dx = ex - topLeftOffset.dx;
          final nextWidth = imgDrawable.size.width - dx;
          if (nextWidth > 0) {
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                offset: imgDrawable.offset + Offset(dx, 0),
                size: Size(nextWidth, imgDrawable.size.height)
            ));
          }
        } break;
        case ResizingPosition.Right: {
          final topRightOffset = imgDrawable.getBound().topRight;
          final dx = ex - topRightOffset.dx;
          final nextWidth = imgDrawable.size.width + dx;
          if (nextWidth > 0) {
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(nextWidth, imgDrawable.size.height)
            ));
          }
        } break;
        case ResizingPosition.BottomLeft: {
          final aspectRatio = imgDrawable.getBound().width / imgDrawable.getBound().height;
          final bottomLeftOffset = imgDrawable.getBound().bottomLeft;
          final dx = ex - bottomLeftOffset.dx;
          final dwidth = -dx;
          final dheight = dwidth / aspectRatio;
          final nextWidth = imgDrawable.size.width + dwidth;
          final nextHeight = imgDrawable.size.height + dheight;
          if (nextWidth > 0 && nextHeight > 0) {
            final nextOffset = imgDrawable.getBound().topLeft + Offset(-dwidth, 0);
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(nextWidth, nextHeight),
                offset: nextOffset
            ));
          }
        } break;
        case ResizingPosition.Bottom: {
          final bottomLeftOffset = imgDrawable.getBound().bottomLeft;
          final dy = ey - bottomLeftOffset.dy;
          final nextHeight = imgDrawable.size.height + dy;
          if (nextHeight > 0) {
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(imgDrawable.size.width, nextHeight)
            ));
          }
        } break;
        case ResizingPosition.BottomRight: {
          final aspectRatio = imgDrawable.getBound().width / imgDrawable.getBound().height;
          final oriTopLeft = imgDrawable.getBound().topLeft;
          final dx = ex - oriTopLeft.dx;
          final nextWidth = dx;
          final nextHeight = nextWidth / aspectRatio;
          if (nextWidth > 0 && nextHeight > 0) {
            yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
                size: Size(nextWidth, nextHeight)
            ));
          }
        } break;
        default: break;
      }
    }
  }

  Stream<DrawingAreaState> _handleUndoDrawing(UndoDrawing event) async* {
    final lastItemInDrawables = state.penDrawableList.set.last;
    yield* _removeFromDrawableListHelper(lastItemInDrawables.drawable);
    yield state.copyWith(
      undoDrawables: state.undoDrawableList.copyWith((list) {
          list.add(DrawableEntry(lastItemInDrawables.drawable));
      })
    );
  }

  Stream<DrawingAreaState> _handleRedoDrawing(RedoDrawing event) async* {
    final lastItemInUndoDrawables = state.undoDrawableList.list.last;
    yield state.copyWith(
      undoDrawables: state.undoDrawableList.copyWith((list) {
          list.remove(lastItemInUndoDrawables);
      })
    );
    yield* _addToDrawableListHelper(lastItemInUndoDrawables.drawable);
  }

  Stream<DrawingAreaState> _handleEraser(double x, double y, int pointer) async* {
    final actionPoint = Point<double>(x, y);

    final List<DrawableEntry> candidates = List<DrawableEntry>();
    for (final item in state.penDrawableList.set) {
      final drawable = item.drawable;
      if (drawable is PenDrawable) {
        // performance optimization
        if (!!!drawable.getBound().contains(Offset(actionPoint.x, actionPoint.y))) {
          continue;
        }

        final drawablePoints = drawable.points;
        for (int i = 1; i < drawablePoints.length; i++) {
          final Point start = drawablePoints[i - 1];
          final Point end = drawablePoints[i];
          final dist = GeometryCalculation.pointToLineDistanceSquare(start, end, actionPoint);
          if (dist <= 25.0) {
            candidates.add(item);
            break;
          }
        }
      }
    }
    for (final cd in candidates) {
      yield* _removeFromDrawableListHelper(cd.drawable);
    }
    
    //
    // Handle TextDrawable
    // 
    candidates.clear();
    for (final item in state.textDrawableList.set) {
      final drawable = item.drawable;
      if (drawable is TextDrawable) {
        if (drawable.getBound().contains(Offset(actionPoint.x, actionPoint.y))) {
          candidates.add(item);
        }
      }
    }
    for (final cd in candidates) {
      final drawable = cd.drawable;
      if (drawable is TextDrawable) {
        yield* _removeFromDrawableListHelper(drawable);
        // find converted-text model
        // Note: -1 is a special case groupId
        if (drawable.groupId != -1) {
          final model = _convertedTextRepository.lookup(drawable.groupId);
          _convertedTextRepository.removeByID(model.id);

          for (final rd in model.drawables) {
            yield* _addToDrawableListHelper(rd);
          }
        }
      }
    }

    yield state.copyWith(
      curWorkingPointer: pointer,
    );
  }

  Stream<DrawingAreaState> _handleLongPress(LongPress event) async * {
    final dpb = _drawingPropertiesBloc.state;
    // must select selecting tool and not select other image simultaneously
    if (dpb.curDrawingTool == DrawingTools.Selecting 
        && dpb.curDrawingState == DrawingState.Ready
    ) {
      DrawableEntry selectedDrawableEntry;
      for (final x in state.imageDrawableList.set) {
        final d = x.drawable;
        if (d is ImageDrawable) {
          if (d.getBound().contains(Offset(_areaScaler.getTargetX(event.x), _areaScaler.getTargetY(event.y)))) {
            selectedDrawableEntry = x;
          }
        }
      }

      // select latest image
      ImageDrawable imgDrawable = selectedDrawableEntry.drawable;
      _drawingPropertiesBloc.add(SetDrawingState(state: DrawingState.SelectingImage));
      yield* _updateImageDrawableHelper(selectedDrawableEntry, imgDrawable.copyWith(showControlBox: true));
      yield state.copyWith(
        curWorkingDrawable: EmptyDrawable()
      );
    }
  }

  Stream<DrawingAreaState> _handleAddImage(AddImage event) async* {
    final dpb = _drawingPropertiesBloc.state;
    if (dpb.curDrawingState != DrawingState.Ready
        && dpb.curDrawingState != DrawingState.SelectingImage
    ) {
      return;
    }

    Uint8List imageByteData;
    switch (event.sourceType) {
      case ImageSourceType.Device: {
        imageByteData = await File(event.path).readAsBytes();
      } break;
      case ImageSourceType.Network:
        _globalBloc.add(ShowProgressDialog("Downloading Image ..."));

        http.Response response;
        try {
          response = await http.get(event.path);
        } on SocketException catch (e) {          
          _globalBloc.add(HideProgressDialog());
          return;
        }
        
        _globalBloc.add(HideProgressDialog());  

        if (response.statusCode == 200) {
          imageByteData = response.bodyBytes;
        }
        break;
      default:
        return;
    }

    final imageUint8List = imageByteData.buffer.asUint8List();
    final imageCodec = await instantiateImageCodec(imageUint8List);
    final imageFrame = await imageCodec.getNextFrame();
    final image = imageFrame.image;
    final aspectRatio = image.width / image.height;

    _drawingPropertiesBloc.add(SetDrawingTool(tool: DrawingTools.Selecting));
    _drawingPropertiesBloc.add(SetDrawingState(state: DrawingState.SelectingImage));

    final addingDrawable = ImageDrawable(
      drawableId: _drawableIDProviderService.getID(),
      image: image,
      showControlBox: true,
      offset: Offset(_areaScaler.getTargetX(200), _areaScaler.getTargetY(200)),
      size: Size(_areaScaler.getTargetX(200 * aspectRatio), _areaScaler.getTargetY(200))
    );

    final addingDrawableEntry = DrawableEntry(addingDrawable);

    // deselect previous image
    if (!!!(state.curSelectingImageDrawableEntry.drawable is EmptyDrawable)) {
      yield* _imageDeselectHelper(state.curSelectingImageDrawableEntry);
    }

    yield state.copyWith(
      imageDrawableList: state.imageDrawableList.copyWith((list) {
        list.add(addingDrawableEntry);
      }),
      curSelectingImageDrawableEntry: addingDrawableEntry
    );
  }

  Stream<DrawingAreaState> _handleDeleteImage(DeleteImage event) async* {
    final dpb = _drawingPropertiesBloc.state;
    if (dpb.curDrawingState == DrawingState.SelectingImage
        && !!!(state.curSelectingImageDrawableEntry.drawable is EmptyDrawable)
    ) {
      yield state.copyWith(
        imageDrawableList: state.imageDrawableList.copyWith((list) {
          list.remove(state.curSelectingImageDrawableEntry);
        }),
        curSelectingImageDrawableEntry: DrawableEntry(EmptyDrawable()),
      );
      _drawingPropertiesBloc.add(SetDrawingState(state: DrawingState.Ready));
    }
  }

  Stream<DrawingAreaState> _handleRotateLeftImage(RotateLeftImage event) async* {
    final dpb = _drawingPropertiesBloc.state;
    if (dpb.curDrawingState == DrawingState.SelectingImage
        && !!!(state.curSelectingImageDrawableEntry.drawable is EmptyDrawable)
    ) {
      final imgEntry = state.curSelectingImageDrawableEntry;
      ImageDrawable imgDrawable = imgEntry.drawable;
      yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
        image: await ImageRotator.rotateLeft(imgDrawable.image),
        size: Size(imgDrawable.size.height, imgDrawable.size.width)
      ));
    }
  }

  Stream<DrawingAreaState> _handleRotateRightImage(RotateRightImage event) async* {
    final dpb = _drawingPropertiesBloc.state;
    if (dpb.curDrawingState == DrawingState.SelectingImage
        && !!!(state.curSelectingImageDrawableEntry.drawable is EmptyDrawable)
    ) {
      final imgEntry = state.curSelectingImageDrawableEntry;
      ImageDrawable imgDrawable = imgEntry.drawable;
      yield* _updateImageDrawableHelper(imgEntry, imgDrawable.copyWith(
          image: await ImageRotator.rotateRight(imgDrawable.image),
          size: Size(imgDrawable.size.height, imgDrawable.size.width)
      ));
    }
  }

  //
  // HELPERS
  //
  Stream<DrawingAreaState> _updateImageDrawableHelper(DrawableEntry originalEntry, ImageDrawable modifiedDrawable, [Drawable selectingImageDrawable]) async* {
    final nextDrawableEntry = DrawableEntry(modifiedDrawable);
    yield state.copyWith(
      imageDrawableList: state.imageDrawableList.copyWith((list) {
        // for SplayTreeSet
        list.remove(originalEntry);
        list.add(DrawableEntry(modifiedDrawable));
      }),
      curSelectingImageDrawableEntry: selectingImageDrawable != null ? DrawableEntry(selectingImageDrawable) : nextDrawableEntry
    );
  }

  Stream<DrawingAreaState> _handleSetDrawableListFromOpenStaticFile(SetDrawableListFromOpenStaticFile event) async* {
    _globalBloc.add(ShowProgressDialog("Initializing ..."));
    await _wordGroupingService.reset();
    await _drawableGroupRepository.reset();
    await _convertedTextRepository.reset();
    await _groupQueueService.reset();

    _drawableIDProviderService.dangerousSetID(event.latestDrawableId);

    yield DrawingAreaState.initial().copyWith(
      drawingAreaSize: state.drawingAreaSize
    );

    for (final d in event.imageDrawables) {
      yield* _addToDrawableListHelper(d);
    }
    
    for (final d in event.textDrawables) {
      yield* _addToDrawableListHelper(d);
    }

    for (final d in event.penDrawables) {
      yield* _addToDrawableListHelper(d);
    }

    _globalBloc.add(HideProgressDialog());
  }

  // Helper Function
  Stream<DrawingAreaState> _imageDeselectHelper(DrawableEntry entry) async* {
    ImageDrawable imgDrawable = entry.drawable;
    yield* _updateImageDrawableHelper(entry, imgDrawable.copyWith(showControlBox: false), EmptyDrawable());
  }

  Stream<DrawingAreaState> _addToDrawableListHelper(Drawable drawable) async* {
    if (drawable is PenDrawable) {
      if (drawable.isConvertingPen) {
        if (drawable.getBound().width >= 1 && drawable.getBound().height >= 1) {
          // need to be ordered calling.
          yield state.copyWith(
            penDrawableList: state.penDrawableList.copyWith((list) {
              list.add(DrawableEntry(drawable));
            })
          );
          _wordGroupingService.notifyAdd(drawable);
        }
      } else {
        yield state.copyWith(
          penDrawableList: state.penDrawableList.copyWith((list) {
            list.add(DrawableEntry(drawable));
          })
        );
      }
    } else if (drawable is TextDrawable) {
      yield state.copyWith(
        textDrawableList: state.textDrawableList.copyWith((list) {
          list.add(DrawableEntry(drawable));
        }),
      );
    }
    else if (drawable is ImageDrawable) {
      yield state.copyWith(
        imageDrawableList: state.imageDrawableList.copyWith((list) {
          list.add(DrawableEntry(drawable));
        }),
      );
    }
  }

  Stream<DrawingAreaState> _removeFromDrawableListHelper(Drawable drawable) async* {   
    if (drawable is PenDrawable) {
      if (drawable.isConvertingPen) {
        _wordGroupingService.notifyDelete(
          DrawingAreaBlocDrawableGrabber(state.penDrawableList.set),
          drawable.id
        );
      }

      yield state.copyWith(
        penDrawableList: state.penDrawableList.copyWith((list) {
          list.remove(DrawableEntry(drawable));
        })
      );
    } else if (drawable is TextDrawable) {
      yield state.copyWith(
        textDrawableList: state.textDrawableList.copyWith((list) {
          list.remove(DrawableEntry(drawable));
        })
      );
    } else if (drawable is ImageDrawable) {
      yield state.copyWith(
        imageDrawableList: state.imageDrawableList.copyWith((list) {
          list.remove(DrawableEntry(drawable));
        })
      );
    } 
  }
}