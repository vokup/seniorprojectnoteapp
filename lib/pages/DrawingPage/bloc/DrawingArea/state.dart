import 'dart:ui';

import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/drawables/EmptyDrawable.dart';
import 'package:senior_project_note_app/libs/ImageResizingResolver.dart';
import 'package:senior_project_note_app/libs/LinkedListExt.dart';
import 'package:senior_project_note_app/libs/SplayTreeSetExt.dart';

@immutable
class DrawingAreaState {
  // to prevent more than 2 finger
  final int curWorkingPointer;
  final Drawable curWorkingDrawable;
  final LinkedListExt<DrawableEntry> undoDrawableList;

  // these 3 lists must be in order.
  final SplayTreeSetExt<DrawableEntry> penDrawableList;
  final SplayTreeSetExt<DrawableEntry> textDrawableList;
  final SplayTreeSetExt<DrawableEntry> imageDrawableList;

  // properties
  // Iterable<DrawableEntry> get drawables => drawableList.set;
  Iterable<DrawableEntry> get drawables => imageDrawableList.set
                                            .followedBy(textDrawableList.set)
                                            .followedBy(penDrawableList.set);

  bool get isRedoable => undoDrawableList.list.length > 0;
  //
  //  for ImageDrawable
  //
  // used for selecting image
  final DrawableEntry curSelectingImageDrawableEntry;
  // used for image resizing
  final ResizingPosition curResizingPosition;

  final Size drawingAreaSize;

  DrawingAreaState({
    @required this .curWorkingDrawable,
    @required LinkedListExt<DrawableEntry> undoDrawables,
    @required this.penDrawableList,
    @required this.textDrawableList,
    @required this.imageDrawableList,
    @required this.curWorkingPointer,
    @required this.curSelectingImageDrawableEntry,
    @required this.curResizingPosition,
    @required this.drawingAreaSize
  }):
  undoDrawableList = undoDrawables;

  DrawingAreaState copyWith({
    Drawable curWorkingDrawable,
    SplayTreeSetExt<DrawableEntry> penDrawableList,
    SplayTreeSetExt<DrawableEntry> textDrawableList,
    SplayTreeSetExt<DrawableEntry> imageDrawableList,
    LinkedListExt<DrawableEntry> undoDrawables,
    int curWorkingPointer,
    DrawableEntry curSelectingImageDrawableEntry,
    ResizingPosition curResizingPosition,
    Size drawingAreaSize
  }) {
    return DrawingAreaState(
      curWorkingDrawable: curWorkingDrawable ?? this.curWorkingDrawable,
      penDrawableList: penDrawableList ?? this.penDrawableList,
      textDrawableList: textDrawableList ?? this.textDrawableList,
      imageDrawableList: imageDrawableList ?? this.imageDrawableList,
      undoDrawables: undoDrawables ?? this.undoDrawableList,
      curWorkingPointer: curWorkingPointer ?? this.curWorkingPointer,
      curSelectingImageDrawableEntry: curSelectingImageDrawableEntry ?? this.curSelectingImageDrawableEntry,
      curResizingPosition: curResizingPosition ?? this.curResizingPosition,
      drawingAreaSize: drawingAreaSize ?? this.drawingAreaSize
    );
  }

  factory DrawingAreaState.initial() {
    return DrawingAreaState(
      penDrawableList: SplayTreeSetExt<DrawableEntry>.empty((k1, k2) => k1.drawable.id - k2.drawable.id),
      textDrawableList: SplayTreeSetExt<DrawableEntry>.empty((k1, k2) => k1.drawable.id - k2.drawable.id),
      imageDrawableList: SplayTreeSetExt<DrawableEntry>.empty((k1, k2) => k1.drawable.id - k2.drawable.id),
      undoDrawables: LinkedListExt<DrawableEntry>.empty(),
      curWorkingDrawable: EmptyDrawable(),
      curWorkingPointer: -1,
      curSelectingImageDrawableEntry: DrawableEntry(EmptyDrawable()),
      curResizingPosition: ResizingPosition.None,
      drawingAreaSize: Size.zero
    );
  }
}