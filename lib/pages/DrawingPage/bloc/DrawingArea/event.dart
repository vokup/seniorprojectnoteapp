import 'dart:ui';

import 'package:equatable/equatable.dart';
import 'package:flutter/gestures.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/ImageSourceType.dart';

abstract class DrawingAreaEvent extends Equatable {
  @override
  List<Object> get props => _props;
  final List _props;

  DrawingAreaEvent([List props = const []]):
      _props = props;
}

class OnPageCreated extends DrawingAreaEvent {
  OnPageCreated(): super();
}

class OnPageResumed extends DrawingAreaEvent {
  OnPageResumed(): super();
}

class OnPagePaused extends DrawingAreaEvent {
  OnPagePaused(): super();
}

class PointerDown extends DrawingAreaEvent {
  final double x;
  final double y;
  final double dx;
  final double dy;
  final PointerDeviceKind kind;
  final int pointer;

  PointerDown({ 
    @required this.x, 
    @required this.y, 
    @required this.dx,
    @required this.dy,
    @required this.kind,
    @required this.pointer 
  }): super([ x, y, dx, dy, kind, pointer ]);
}

class PointerUp extends DrawingAreaEvent {
  final double x;
  final double y;
  final double dx;
  final double dy;
  final PointerDeviceKind kind;
  final int pointer;
  final int pointerCount;

  PointerUp({ 
    @required this.x, 
    @required this.y, 
    @required this.dx,
    @required this.dy,
    @required this.kind,
    @required this.pointer,
    @required this.pointerCount
  }): super([ x, y, dx, dy, kind, pointer, pointerCount ]);
}

class PointerMove extends DrawingAreaEvent {
  final double x;
  final double y;
  final double dx;
  final double dy;
  final PointerDeviceKind kind;
  final int pointer;

  PointerMove({ 
    @required this.x, 
    @required this.y, 
    @required this.dx,
    @required this.dy,
    @required this.kind,
    @required this.pointer
  }): super([ x, y, dx, dy, kind, pointer ]);
}

class LongPress extends DrawingAreaEvent {
  final double x;
  final double y;

  LongPress({
    @required this.x,
    @required this.y
  });
}

class RedoDrawing extends DrawingAreaEvent {
  RedoDrawing(): super();
}
class UndoDrawing extends DrawingAreaEvent {
  UndoDrawing(): super();
}

class AddImage extends DrawingAreaEvent {
  final String path;
  final ImageSourceType sourceType;

  AddImage({
    @required this.path,
    @required this.sourceType
  }): super([ path, sourceType ]);
}

class DeleteImage extends DrawingAreaEvent { }
class RotateLeftImage extends DrawingAreaEvent { }
class RotateRightImage extends DrawingAreaEvent { }

class SetDrawableListFromOpenStaticFile extends DrawingAreaEvent {
  final int latestDrawableId;
  final Iterable<Drawable> penDrawables;
  final Iterable<Drawable> textDrawables;
  final Iterable<Drawable> imageDrawables;

  SetDrawableListFromOpenStaticFile({
    @required this.latestDrawableId,
    @required this.penDrawables,
    @required this.textDrawables,
    @required this.imageDrawables
  }): super([ latestDrawableId, penDrawables, textDrawables, imageDrawables ]);
}

class AddConvertedText extends DrawingAreaEvent {
  final String text;
  final Offset offset;
  final Rect renderedBound;
  final double fontSize;
  final Color color;
  final Iterable<Drawable> relatedDrawables;
  final int groupId;

  AddConvertedText(
    this.text, 
    this.fontSize, 
    this.offset, 
    this.color,
    this.renderedBound, 
    this.relatedDrawables,
    this.groupId
  ): super([ text, offset, renderedBound, fontSize, color, relatedDrawables, groupId ]);
}

class SetDrawingAreaSize extends DrawingAreaEvent {
  final double width;
  final double height;
  SetDrawingAreaSize(this.width, this.height): super([ width, height ]);
}

class MoveLayerUp extends DrawingAreaEvent { }
class MoveLayerDown extends DrawingAreaEvent { }