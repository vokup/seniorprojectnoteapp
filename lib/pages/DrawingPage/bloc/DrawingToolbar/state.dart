import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
class DrawingToolbarState {
  final bool isRotateLeftButtonEnabled;
  final bool isRotateRightButtonEnabled;
  final bool isDeleteImageButtonEnabled;
  final bool isAddImageButtonEnabled;

  final bool isUndoButtonEnabled;
  final bool isRedoButtonEnabled;

  final String documentName;

  DrawingToolbarState({
    @required this.isRotateLeftButtonEnabled,
    @required this.isRotateRightButtonEnabled,
    @required this.isDeleteImageButtonEnabled,
    @required this.isAddImageButtonEnabled,

    @required this.isUndoButtonEnabled,
    @required this.isRedoButtonEnabled,

    @required this.documentName
  });

  DrawingToolbarState copyWith({
    bool isRotateLeftButtonEnabled,
    bool isRotateRightButtonEnabled,
    bool isDeleteImageButtonEnabled,
    bool isAddImageButtonEnabled,

    bool isUndoButtonEnabled,
    bool isRedoButtonEnabled,

    String documentName
  }) {
    return DrawingToolbarState(
      isRotateLeftButtonEnabled: isRotateLeftButtonEnabled ?? this.isRotateLeftButtonEnabled,
      isRotateRightButtonEnabled: isRotateRightButtonEnabled ?? this.isRotateRightButtonEnabled,
      isDeleteImageButtonEnabled: isDeleteImageButtonEnabled ?? this.isDeleteImageButtonEnabled,
      isAddImageButtonEnabled: isAddImageButtonEnabled ?? this.isAddImageButtonEnabled,
      isUndoButtonEnabled: isUndoButtonEnabled ?? this.isUndoButtonEnabled,
      isRedoButtonEnabled: isRedoButtonEnabled ?? this.isRedoButtonEnabled,
      documentName: documentName ?? this.documentName
    );
  }

  factory DrawingToolbarState.initial() {
    return DrawingToolbarState(
        isRotateLeftButtonEnabled: false,
        isRotateRightButtonEnabled: false,
        isDeleteImageButtonEnabled: false,
        isAddImageButtonEnabled: true,
        isRedoButtonEnabled: false,
        isUndoButtonEnabled: false,
        documentName: "Untitled"
    );
  }
}