import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingState.dart';

class DrawingToolbarBloc extends Bloc<DrawingToolbarEvent, DrawingToolbarState> {

  StreamSubscription _drawingAreaBlocSubscription;

  @override
  DrawingToolbarState get initialState => DrawingToolbarState.initial();

  @override
  Stream<DrawingToolbarState> mapEventToState(DrawingToolbarEvent event) async* {
    if (event is NotifyDrawingStateChange) {
      final drawingState = event.drawingState;
      switch (drawingState) {
        case DrawingState.SelectingImage: {
          yield state.copyWith(
            isDeleteImageButtonEnabled: true,
            isRotateLeftButtonEnabled: true,
            isRotateRightButtonEnabled: true
          );
        } break;
        default: {
          yield state.copyWith(
              isDeleteImageButtonEnabled: false,
              isRotateLeftButtonEnabled: false,
              isRotateRightButtonEnabled: false
          );
        } break;
      }
    } else if (event is SetUndoButtonEnabled) {
      yield state.copyWith(
        isUndoButtonEnabled: event.isEnabled
      );
    } else if (event is SetRedoButtonEnabled) {
      yield state.copyWith(
        isRedoButtonEnabled: event.isEnabled
      );
    } else if (event is UpdateDocumentName) {
      yield state.copyWith(
        documentName: event.name
      );
    }
  }

  void listenToDrawingAreaBloc(DrawingAreaBloc bloc) {
    _drawingAreaBlocSubscription = bloc.listen((state) {

      bool isUndoable = state.penDrawableList.set.length > 0;
      if (this.state.isUndoButtonEnabled != isUndoable) {
        this.add(SetUndoButtonEnabled(isUndoable));
      }

      if (this.state.isRedoButtonEnabled != state.isRedoable) {
        this.add(SetRedoButtonEnabled(state.isRedoable));
      }
    });
  }

  @override
  Future<void> close() {
    return Future.wait<void>([ 
      super.close(), 
      _drawingAreaBlocSubscription.cancel() 
    ]);
  }
}