import 'package:equatable/equatable.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingState.dart';

class DrawingToolbarEvent extends Equatable {
  @override
  List<Object> get props => _props;
  final List _props;

  DrawingToolbarEvent([List props = const []]):
        _props = props;
}

class NotifyDrawingStateChange extends DrawingToolbarEvent {
  final DrawingState drawingState;
  NotifyDrawingStateChange(this.drawingState): super([ drawingState ]);
}

class SetRedoButtonEnabled extends DrawingToolbarEvent {
  final bool isEnabled;
  SetRedoButtonEnabled(this.isEnabled): super([ isEnabled ]);
}

class SetUndoButtonEnabled extends DrawingToolbarEvent {
  final bool isEnabled;
  SetUndoButtonEnabled(this.isEnabled): super([ isEnabled ]);
}

class UpdateDocumentName extends DrawingToolbarEvent {
  final String name;
  UpdateDocumentName(this.name): super([ name ]);
}