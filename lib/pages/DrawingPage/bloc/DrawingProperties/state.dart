import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingState.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingTool.dart';

@immutable
class DrawingPropertiesState {
  final DrawingState curDrawingState;
  final DrawingTools curDrawingTool;
  final Color curDrawingColor;
  final double curDrawingThickness;

  DrawingPropertiesState({
    @required this.curDrawingState,
    @required this.curDrawingTool,
    @required this.curDrawingColor,
    @required this.curDrawingThickness
  });

  DrawingPropertiesState copyWith({
    DrawingState drawingState,
    DrawingTools drawingTool,
    Color drawingColor,
    double drawingThickness
  }) {
    return DrawingPropertiesState(
      curDrawingState: drawingState ?? this.curDrawingState,
      curDrawingTool: drawingTool ?? this.curDrawingTool,
      curDrawingColor: drawingColor ?? this.curDrawingColor,
      curDrawingThickness: drawingThickness ?? this.curDrawingThickness
    );
  }

  factory DrawingPropertiesState.initial() {
    return DrawingPropertiesState(
      curDrawingState: DrawingState.Ready,
      curDrawingColor: Colors.black, 
      curDrawingThickness: 1.0, 
      curDrawingTool: DrawingTools.NormalPen
    );
  }
}