import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingState.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/DrawingTool.dart';

class DrawingPropertiesEvent extends Equatable {
  @override
  List<Object> get props => _props;
  final List _props;

  DrawingPropertiesEvent([List props = const []]):
        _props = props;
}

class SetDrawingState extends DrawingPropertiesEvent {
  final DrawingState state;
  SetDrawingState({ @required this.state }): super([ state ]);
}

class SetDrawingTool extends DrawingPropertiesEvent {
  final DrawingTools tool;
  SetDrawingTool({ @required this.tool }): super([ tool ]);
}

class SetDrawingColor extends DrawingPropertiesEvent {
  final Color color;
  SetDrawingColor({ @required this.color }): super([ color ]);
}

class SetDrawingThickness extends DrawingPropertiesEvent {
  final double thickness;
  SetDrawingThickness({ @required this.thickness }): super([ thickness ]);
}