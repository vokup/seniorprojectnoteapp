import 'package:bloc/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/event.dart';

class DrawingPropertiesBloc extends Bloc<DrawingPropertiesEvent, DrawingPropertiesState> {
  @override
  DrawingPropertiesState get initialState => DrawingPropertiesState.initial();

  final DrawingToolbarBloc drawingToolbarBloc;

  DrawingPropertiesBloc(this.drawingToolbarBloc);

  @override
  Stream<DrawingPropertiesState> mapEventToState(DrawingPropertiesEvent event) async* {
    if (event is SetDrawingState) {
      print("DrawingState: ${event.state}");
      drawingToolbarBloc.add(NotifyDrawingStateChange(event.state));
      yield state.copyWith(drawingState: event.state);
    }

    if (event is SetDrawingTool) {
      yield state.copyWith(drawingTool: event.tool);
    }

    if (event is SetDrawingColor) {
      yield state.copyWith(drawingColor: event.color);
    }

    if (event is SetDrawingThickness) {
      yield state.copyWith(drawingThickness: event.thickness);
    }
  }
}