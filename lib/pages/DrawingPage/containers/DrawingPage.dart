import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior_project_note_app/drawables/Drawable.dart';
import 'package:senior_project_note_app/drawables/DrawablePainter.dart';
import 'package:senior_project_note_app/libs/DrawingAreaScaler.dart';
import 'package:senior_project_note_app/libs/LinkedListExt.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingControlPanel/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingControlPanel/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingControlPanel/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingProperties/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/widgets/DrawingPageWidget.dart';
import 'package:senior_project_note_app/pages/DrawingPage/widgets/DrawingSideControlPanel.dart';
import 'package:senior_project_note_app/utils/IterableConverter.dart';
import 'package:senior_project_note_app/widgets/DrawingWidget.dart';

class DrawingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DrawingPageState();
}

class _DrawingPageState extends State<DrawingPage> with WidgetsBindingObserver {
  final _drawablePainterKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    WidgetsBinding.instance.addPostFrameCallback((duration) {
      final dab = BlocProvider.of<DrawingAreaBloc>(context);
      final RenderBox box = _drawablePainterKey.currentContext.findRenderObject();
      dab.add(SetDrawingAreaSize(box.size.width, box.size.height));
    });

    print("DrawingPageState: initState");

    if (this.mounted) {
      final dab = BlocProvider.of<DrawingAreaBloc>(context);
      print("DrawingPageState: OnPageStart");
      dab.add(OnPageCreated());
    }
  }

  @override
  void deactivate() {
    super.deactivate();
    final dab = BlocProvider.of<DrawingAreaBloc>(context);
    dab.add(OnPagePaused());
    print("DrawingPageState: deactivate");
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    final dab = BlocProvider.of<DrawingAreaBloc>(context);
    dab.add(OnPagePaused());
    print("DrawingPageState: dispose");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);  
    if (state == AppLifecycleState.paused) {
      // went to Background
      final dab = BlocProvider.of<DrawingAreaBloc>(context);
      // dab.add(OnPagePaused());
      print("DrawingPageState: paused");
    }
    else if (state == AppLifecycleState.resumed) {
      // came back to Foreground
      final dab = BlocProvider.of<DrawingAreaBloc>(context);
      // dab.add(OnPageResumed());
      print("DrawingPageState: resumed");
    }
  }

  @override
  Widget build(BuildContext context) {
    final dab = BlocProvider.of<DrawingAreaBloc>(context);
    final areaScaler = RepositoryProvider.of<AreaScaler>(context);

    return BlocBuilder<DrawingControlPanelBloc, DrawingControlPanelState>(
      builder: (context, dcbState) {
        return DrawingPageWidget(
          isControlPanelSittingLeft: dcbState.isSittingLeft,
          controlPanelPosition: Point<double>(0, 0),
          onDrawingAreaPointerDown: (m) { 
            dab.add(PointerDown(x: m.x, y: m.y, dx: m.dx, dy: m.dy, kind: m.kind, pointer: m.pointer));
          },
          onDrawingAreaPointerUp: (m) {
            dab.add(PointerUp(x: m.x, y: m.y, dx: m.dx, dy: m.dy, kind: m.kind, pointer: m.pointer, pointerCount: m.pointerCount));
          },
          onDrawingAreaPointerMove: (m) { 
            dab.add(PointerMove(x: m.x, y: m.y, dx: m.dx, dy: m.dy, kind: m.kind, pointer: m.pointer));
          },
          onDrawingAreaPointerLongPress: (m) {
            dab.add(LongPress(x: m.x, y: m.y));
          },
          drawingArea: BlocBuilder<DrawingAreaBloc, DrawingAreaState>(
            builder: (context, state) {
              return DrawingWidget(
                customPaint: CustomPaint(
                  key: _drawablePainterKey,
                  painter: DrawablePainter(
                    areaScaler: areaScaler,
                    drawables: IterableConverter<DrawableEntry, Drawable>(
                      state.drawables, 
                      (x) => x.drawable
                    )
                    .followedBy([ state.curWorkingDrawable ])
                  )
                )
              );
            },
          ),
          drawingSideControlPanel: BlocBuilder<DrawingControlPanelBloc, DrawingControlPanelState>(
              builder: (context, state) {
                final dcpb = BlocProvider.of<DrawingControlPanelBloc>(context);
                final dpb = BlocProvider.of<DrawingPropertiesBloc>(context);
                return BlocBuilder<DrawingPropertiesBloc, DrawingPropertiesState>(
                  builder: (context, state) {
                    return DrawingSideControlPanel(
                        isSittingLeft: dcbState.isSittingLeft,
                        curDrawingTool: state.curDrawingTool,
                        drawingColor: state.curDrawingColor,
                        drawingThickness: state.curDrawingThickness,
                        onDrawingToolChange: (t) { dpb.add(SetDrawingTool(tool: t)); },
                        onDrawingColorChange: (c) { dpb.add(SetDrawingColor(color: c)); },
                        onDrawingThicknessChange: (t) { dpb.add(SetDrawingThickness(thickness: t)); },
                        onControlPanelMove: (x, y) { dcpb.add(MoveControlPanelByDelta(x: x, y: y)); },
                        onChangeSittingSideClick: () {
                          final dcb = BlocProvider.of<DrawingControlPanelBloc>(context);
                          dcb.add(ChangeControlPanelSide(isSittingLeft: !!!dcbState.isSittingLeft));
                        },
                    );
                  }
              );
            }
          ),
        );
      }
    );
  }  
}