import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingArea/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/DrawingToolbar/state.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/StaticFile/bloc.dart';
import 'package:senior_project_note_app/pages/DrawingPage/bloc/StaticFile/event.dart';
import 'package:senior_project_note_app/pages/DrawingPage/containers/DrawingPage.dart';
import 'package:senior_project_note_app/pages/DrawingPage/enums/ImageSourceType.dart';
import 'package:senior_project_note_app/pages/DrawingPage/widgets/DrawingPageAppLayoutWidget.dart';
import 'package:senior_project_note_app/widgets/AddImageDialog.dart';

class DrawingPageAppLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final dab = BlocProvider.of<DrawingAreaBloc>(context);
    final dsb = BlocProvider.of<StaticFileBloc>(context);
    final dtb = BlocProvider.of<DrawingToolbarBloc>(context);

    final drawingPage = DrawingPage();

    return BlocBuilder<DrawingToolbarBloc, DrawingToolbarState>(
      builder: (context, state) {
        return DrawingPageAppLayoutWidget(
          // need trigger from DrawingAreaBloc
          isLayerDownButtonEnabled: state.isRotateLeftButtonEnabled,
          isLayerUpButtonEnabled: state.isRotateLeftButtonEnabled,

          isRedoButtonEnable: state.isRedoButtonEnabled,
          isUndoButtonEnable: state.isUndoButtonEnabled,

          isAddImageButtonEnabled: state.isAddImageButtonEnabled,
          isDeleteImageButtonEnabled: state.isDeleteImageButtonEnabled,
          isRotateLeftButtonEnabled: state.isRotateLeftButtonEnabled,
          isRotateRightButtonEnabled: state.isRotateRightButtonEnabled,

          drawingPage: drawingPage,

          documentName: state.documentName,
          onDocumentNameChange: (text) {
            dtb.add(UpdateDocumentName(text));
          },

          onRedoButtonClick: () {
            if (dab.state.isRedoable) {
              dab.add(RedoDrawing());
            }
          },
          onUndoButtonClick: () {
            if (dab.state.drawables.length > 0) {
              dab.add(UndoDrawing());
            }
          },
          onAddImageButtonClick: () {
            showDialog(
              context: context,
              builder: (context) {
                return AddImageDialog(
                  onAddImageFromUrlClick: (url) {
                    dab.add(AddImage(sourceType: ImageSourceType.Network, path: url));
                    Navigator.of(context).pop();
                  },
                  onSelectFromDeviceButtonClick: (url) {
                    dab.add(AddImage(sourceType: ImageSourceType.Device, path: url));
                    Navigator.of(context).pop();
                  },
                );
              }
            );
          },
          onDeleteImageButtonClick: () {
            dab.add(DeleteImage());
          },
          onImageLayerUpButtonClick: () {
            dab.add(MoveLayerUp());
          },
          onImageLayerDownButtonClick: () {
            dab.add(MoveLayerDown());
          },
          onRotateLeftImageButtonClick: () {
            dab.add(RotateLeftImage());
          },
          onRotateRightImageButtonClick: () {
            dab.add(RotateRightImage());
          },
          onOpenStaticFileButtonClick: () async {
            final path = await FilePicker.getFilePath();
            if (path != null && path != "") {
              dsb.add(OpenStaticFile(path));
            }
          },
          onSaveStaticFileButtonClick: (docName) {
            dsb.add(SaveStaticFile(docName));
          },  
          onSavePDFFileButtonClick: (docName) {
            dsb.add(SavePDFFile(docName));
          },
        );
      }
    );
  }
}