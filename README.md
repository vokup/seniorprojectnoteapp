# Senior Project Note Application
### เป็นส่วนหนึ่งของวิชาโครงงานวิศวกรรม สาขาวิศวกรรมคอมพิวเตอร์ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์ วิทยาเขตบางเขน

# รายละเอียดโครงงาน

**Project Name:** Note Application with Handwriting to Text Feature 

**ชื่อโครงงาน:** แอพพลิเคชั่นสําหรับจดบันทึกโดยการแปลงลายมือเป็นตัวอักษร

**อาจารย์ที่ปรึกษา:** รศ.ดร.จันทนา  จันทราพรชัย

**ผู้จัดทำ**

    1. นาย ภัทรพล ร่มเพชร       5910500376

    2. นาย ธนปวีณ์ พระจันทร์     5910503740 

# วิดีโอการนำเสนอโครงงาน
[![](http://img.youtube.com/vi/pdO0TCugHkM/0.jpg)](http://www.youtube.com/watch?v=pdO0TCugHkM "วิดีโอการนำเสนอโครงาน")

# Details
- **Language:** Dart 2, Java
- **Framework:** Flutter
- **Library**
    - Tensorflow Lite
